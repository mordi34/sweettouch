package com.mordiwes.SweetTouch.visualisation3d;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.opengl.Matrix;
import android.os.AsyncTask;
import com.mordiwes.SweetTouch.async.TaskCallback;
import com.mordiwes.SweetTouch.display.*;
import com.mordiwes.SweetTouch.elements.Atom;
import com.mordiwes.SweetTouch.elements.Molecule;
import com.mordiwes.SweetTouch.elements.Ring;
import com.mordiwes.SweetTouch.ui.MainActivity;
import com.mordiwes.SweetTouch.util.Constants;
import com.mordiwes.SweetTouch.util.PeriodicTable;
import com.mordiwes.SweetTouch.util.Vector3d;
import rajawali.BaseObject3D;
import rajawali.lights.DirectionalLight;
import rajawali.materials.*;
import rajawali.math.Number3D;
import rajawali.primitives.Line3D;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

/**
 * Created by Mordechai on 2014-09-18.
 */
public class Renderer3D extends SweetTouchRenderer implements TaskCallback<Boolean> {
    public static final String tag = "Renderer3D";

    private final float STANDARD_BALL_RADIUS = 0.2f;
    private final float STANDARD_LINE_THICKNESS = 10f;

    private final float CAMERA_DEFAULT_X = 0f;
    private final float CAMERA_DEFAULT_Y = 0f;
    private final float CAMERA_DEFAULT_Z = 20f;

    private float ROTATION_SPEED_REDUCTION_FACTOR = 3f;

    private DirectionalLight mLight;
    private DirectionalLight mLight2;

    /**
     * Store the accumulated rotation.
     */
    private final float[] mCurrentRotation = new float[16];
    private final float[] mTemporaryMatrix = new float[16];

    private SweetTouchBaseObject3D groupAll = new SweetTouchBaseObject3D();
    private SweetTouchBaseObject3D groupBalls = new SweetTouchBaseObject3D();
    private SweetTouchBaseObject3D groupSticks = new SweetTouchBaseObject3D();
    private SweetTouchBaseObject3D groupPaperChainSticks = new SweetTouchBaseObject3D();
    private SweetTouchBaseObject3D groupAxes = new SweetTouchBaseObject3D();

    // private PhongMaterial material = new PhongMaterial();

    private boolean hasSetupBallsAndSticks = false;
    private boolean hasSetupPaperChain = false;
    private boolean hasResizedBalls = false;

    private boolean newRequestToRenderBallAndStick = false;
    private boolean newRequestToRenderBallsAsPoints = false;
    private boolean newRequestToRenderOnlySticks = false;
    private boolean newRequestToRenderCpkModel = false;
    private boolean newRequestToRenderPaperChain = false;

    public Renderer3D(Context context) {
        super(context, 60);
    }

    protected void initScene() {
//        setFogEnabled(true);
        setUpLights();
        setUpCamera();

    }

    private void setUpLights() {
        mLight = new DirectionalLight(1f, 0.2f, -1.0f); // set the direction
        mLight2 = new DirectionalLight(-1f, -0.2f, 1.0f); // set the direction

        mLight.setColor(1.0f, 1.0f, 1.0f);
        mLight2.setColor(1.0f, 1.0f, 1.0f);

        mLight.setPosition(100, 100, 100);
        mLight.setPosition(-100, -100, -100);

        mLight.setPower(1);
        mLight2.setPower(1);
    }

    private void setUpCamera() {
        mCamera.setX(CAMERA_DEFAULT_X);
        mCamera.setY(CAMERA_DEFAULT_Y);
        mCamera.setZ(CAMERA_DEFAULT_Z);
        mCamera.setFogNear(10);
        mCamera.setFogFar(30);
        mCamera.setFogColor(Color.argb(50, 0, 0, 0));
    }

    private void setUpAxes() {
        Number3D viewPortCorner = getInWorldCoordinates(mViewportWidth, mViewportHeight);
        viewPortCorner.x *= -1;
        viewPortCorner.y *= -1;
        viewPortCorner.x += 2;
        viewPortCorner.y -= 5;
        Number3D axesLocation = viewPortCorner;
        System.out.println("Location: " + viewPortCorner);

        Stack<Number3D> points = new Stack<Number3D>();
        points.push(new Number3D(axesLocation.x, axesLocation.y + 1, axesLocation.z));
        points.push(new Number3D(axesLocation.x, axesLocation.y, axesLocation.z));

        Line3D bond = new Line3D(points, STANDARD_LINE_THICKNESS, Color.RED);
        SimpleMaterial bondMaterial = new SimpleMaterial();
        bond.setMaterial(bondMaterial);
        bond.setColor(Color.RED);
        groupAxes.addChild(bond);

        points = new Stack<Number3D>();
        points.push(new Number3D(axesLocation.x + 1, axesLocation.y, axesLocation.z));
        points.push(new Number3D(axesLocation.x, axesLocation.y, axesLocation.z));

        bond = new Line3D(points, STANDARD_LINE_THICKNESS, Color.RED);
        bondMaterial = new SimpleMaterial();
        bond.setMaterial(bondMaterial);
        bond.setColor(Color.GREEN);
        groupAxes.addChild(bond);


        points = new Stack<Number3D>();
        points.push(new Number3D(axesLocation.x, axesLocation.y, axesLocation.z + 1));
        points.push(new Number3D(axesLocation.x, axesLocation.y, axesLocation.z));

        bond = new Line3D(points, STANDARD_LINE_THICKNESS, Color.RED);
        bondMaterial = new SimpleMaterial();
        bond.setMaterial(bondMaterial);
        bond.setColor(Color.BLUE);
        groupAxes.addChild(bond);

        groupAxes.setPosition(axesLocation);

    }

    private void setUpGroups() {
        groupBalls.addLight(mLight);
        // groupBalls.addLight(mLight2);

        groupSticks.addLight(mLight);
        // groupSticks.addLight(mLight2);

//        setUpAxes();
//        groupAxes.addLight(mLight);
//        addChild(groupAxes);

        groupAll.addLight(mLight);


        //  groupAll.addLight(mLight2);

        groupAll.addChild(groupBalls);
        groupAll.addChild(groupSticks);


    }

    private void setUpBallsAndSticks() {
        if (MainActivity.structure == null) {
            return;
        }

        Activity activity = (Activity) mContext;
        Canvas3D canvas = (Canvas3D) activity.getFragmentManager().findFragmentByTag(Canvas3D.FRAGMENT_TAG);
        canvas.showLoader();

        new Thread(
                new Runnable() {
                    public void run() {
                        if (MainActivity.structure == null) {
                            return;
                        }

                        for (Molecule molecule : MainActivity.structure.getMolecules()) {

                            for (Atom atom : molecule.getAtoms()) {

                                // SimpleMaterial material = new SimpleMaterial();
                                // DiffuseMaterial material = new DiffuseMaterial();
                                // SphereMapMaterial material = new SphereMapMaterial();
                                PhongMaterial material = new PhongMaterial();
                                // GouraudMaterial material = new GouraudMaterial();

                                if (originShift == null) {
                                    findOriginShift(atom.getLocation());
                                }

                                AtomSphere ball = new AtomSphere(STANDARD_BALL_RADIUS, 9, 9, atom);
                                ball.setMaterial(material);
                                ball.getMaterial().setUseColor(true);
                                Number3D ballRenderPosition = calculateShiftedPosition(atom.getLocation(), originShift);
                                ball.setPosition(ballRenderPosition);
                                ball.setColor(PeriodicTable.getColour(atom));

                                groupBalls.addChild(ball);

                                for (Atom neighbour : atom.getNeighbours()) {

                                    Number3D neighborPos = calculateShiftedPosition(neighbour.getLocation(), originShift);
                                    Number3D midPoint = getMidPoint(ballRenderPosition, neighborPos);

                                    Stack<Number3D> points = new Stack<Number3D>();
                                    points.push(ballRenderPosition);
                                    points.push(midPoint);

                                    Line3D bond = new Line3D(points, STANDARD_LINE_THICKNESS, PeriodicTable.getColour(atom));
                                    SimpleMaterial bondMaterial = new SimpleMaterial();
                                    bond.setMaterial(bondMaterial);
                                    bond.setColor(PeriodicTable.getColour(atom));
                                    groupSticks.addChild(bond);

                                    Stack<Number3D> points2 = new Stack<Number3D>();
                                    points2.push(midPoint);
                                    points2.push(neighborPos);

                                    Line3D bond2 = new Line3D(points2, STANDARD_LINE_THICKNESS, PeriodicTable.getColour(neighbour));
                                    SimpleMaterial bondMaterial2 = new SimpleMaterial();
                                    bond2.setMaterial(bondMaterial2);
                                    bond2.setColor(PeriodicTable.getColour(neighbour));
                                    groupSticks.addChild(bond2);
                                }

                            }
                        }
                        hasSetupBallsAndSticks = true;

                        setUpGroups();
                        addChild(groupAll);

                        Activity activity = (Activity) mContext;
                        Canvas3D canvas = (Canvas3D) activity.getFragmentManager().findFragmentByTag(Canvas3D.FRAGMENT_TAG);
                        canvas.hideLoader();

                        displayBallAndStick();

                    }
                }).start();
    }


    private void setUpCpkModel() {
//        new Thread(
//                new Runnable() {
//                    public void run() {

        for (int i = 0; i < groupBalls.getNumChildren(); i++) {
            BaseObject3D object = groupBalls.getChildAt(i);
            // check just in case
            if (object instanceof AtomSphere) {
                AtomSphere ball = (AtomSphere) object;
                ball.setToVdwRadius();
            }
        }
//                    }
//                }).start();

    }

    private void resizeAllBalls(final float radius) {
//        new Thread(
//                new Runnable() {
//                    public void run() {

        for (int i = 0; i < groupBalls.getNumChildren(); i++) {
            BaseObject3D object = groupBalls.getChildAt(i);
            // check just in case
            if (object instanceof AtomSphere) {
                AtomSphere ball = (AtomSphere) object;
                try {
                    ball.setToDisplayRadiusPostInit();
                } catch (IllegalArgumentException ex) {
                    ball.setRadius(radius);
                }
            }
        }
//                    }
//                }).start();

    }

    private Number3D getMidPoint(Number3D pointA, Number3D pointB) {
        return new Number3D((pointA.x + pointB.x) / 2, (pointA.y + pointB.y) / 2, (pointA.z + pointB.z) / 2);
    }

    public void onDrawFrame(final GL10 glUnused) {
        super.onDrawFrame(glUnused);

        if (flagNewRequestToReset) {
            flagNewRequestToReset = false;

            destroyScene();

            groupBalls = new SweetTouchBaseObject3D();
            groupSticks = new SweetTouchBaseObject3D();
            groupPaperChainSticks = new SweetTouchBaseObject3D();
            groupAll = new SweetTouchBaseObject3D();

            mScale = Constants.INITIAL_SCALE;

            hasSetupBallsAndSticks = false;
            hasSetupPaperChain = false;
        }

        if (newRequestToRenderBallAndStick) {
            newRequestToRenderBallAndStick = false;
            renderBallAndStick();

        } else if (newRequestToRenderCpkModel) {
            newRequestToRenderCpkModel = false;
            renderCpkModel();
        } else if (newRequestToRenderBallsAsPoints) {
            newRequestToRenderBallsAsPoints = false;
            renderBallsOnlyAsPoints();
        } else if (newRequestToRenderOnlySticks) {
            newRequestToRenderOnlySticks = false;
            renderSticksOnly();
        } else if (newRequestToRenderPaperChain) {
            newRequestToRenderPaperChain = false;
            renderPaperChain();

        }


    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        super.onSurfaceCreated(gl, config);

    }


    protected void onSwipe(Vector3d position) {
        if (!isScaleInProgress) {
            mSwipeDeltaX = ((float) position.x - (float) mLastTouch.x) / MainActivity.DENSITY / ROTATION_SPEED_REDUCTION_FACTOR;
            mSwipeDeltaY = ((float) position.y - (float) mLastTouch.y) / MainActivity.DENSITY / ROTATION_SPEED_REDUCTION_FACTOR;

            Matrix.setIdentityM(mCurrentRotation, 0);
            Matrix.rotateM(mCurrentRotation, 0, mSwipeDeltaX, 0.0f, 1.0f, 0.0f);
            Matrix.rotateM(mCurrentRotation, 0, mSwipeDeltaY, 1.0f, 0.0f, 0.0f);

            Matrix.multiplyMM(mTemporaryMatrix, 0, mCurrentRotation, 0, groupAll.getAccumulatedRotationsMatrix(), 0);
            System.arraycopy(mTemporaryMatrix, 0, groupAll.getAccumulatedRotationsMatrix(), 0, 16);
        }

//        Matrix.multiplyMM(mTemporaryMatrix, 0, mCurrentRotation, 0, groupAxes.getAccumulatedRotationsMatrix(), 0);
//        System.arraycopy(mTemporaryMatrix, 0, groupAxes.getAccumulatedRotationsMatrix(), 0, 16);

    }

    protected void onDoubleTapSwipe(Vector3d position) {
        moveObjectWithBuffer((float) position.x, (float) position.y, groupAll);
    }

    public boolean reactToDoubleTap(Vector3d position) {
        super.reactToDoubleTap(position);

        findMoveBuffer(position, groupAll);

        return true;
    }


    public void reactToScale(float scaleFactor) {
        isScaleInProgress = true;

        scaleFactor = Math.max(0.9f, Math.min(scaleFactor, 1.1f));

        float tempScale = mScale;
        tempScale *= scaleFactor;

        if (tempScale < Constants.MIN_SCALE || tempScale > Constants.MAX_SCALE) {
            return;
        }

        mScale = tempScale;

        mCamera.setFogNear(mCamera.getFogNear()/scaleFactor);
        mCamera.setFogFar(mCamera.getFogFar() * scaleFactor);

        groupAll.setScale(new Number3D(mScale, mScale, mScale));
    }

    @Override
    public void setAccelerometerValues(float x, float y, int z) {
//        Matrix.setIdentityM(mCurrentRotation, 0);
//        Matrix.rotateM(mCurrentRotation, 0, x, 0.0f, 1.0f, 0.0f);
//        Matrix.rotateM(mCurrentRotation, 0, y, 1.0f, 0.0f, 0.0f);
//
//        Matrix.multiplyMM(mTemporaryMatrix, 0, mCurrentRotation, 0, groupAll.getAccumulatedRotationsMatrix(), 0);
//        System.arraycopy(mTemporaryMatrix, 0, groupAll.getAccumulatedRotationsMatrix(), 0, 16);
    }

    public void displayBallAndStick() {
        newRequestToRenderBallAndStick = true;
    }

    private void renderBallAndStick() {

        if (!hasSetupBallsAndSticks) {
            setUpBallsAndSticks();
//            SetupBallsAndSticksTask sbst = new SetupBallsAndSticksTask(mContext,this);
//            sbst.execute();
        }

        if (hasResizedBalls) {
            resizeAllBalls(STANDARD_BALL_RADIUS);
            hasResizedBalls = false;
        }
        groupBalls.setVisible(true);
        groupSticks.setVisible(true);
        groupPaperChainSticks.setVisible(false);
    }

    public void displayBallsOnlyAsPoints() {
        newRequestToRenderBallsAsPoints = true;
    }

    private void renderBallsOnlyAsPoints() {
        if (!hasSetupBallsAndSticks) {
            setUpBallsAndSticks();
        }

        if (hasResizedBalls) {
            resizeAllBalls(STANDARD_BALL_RADIUS);
            hasResizedBalls = false;
        }
        groupBalls.setVisible(true);
        groupSticks.setVisible(false);
        groupPaperChainSticks.setVisible(false);
    }

    public void displaySticksOnly() {
        newRequestToRenderOnlySticks = true;
    }

    private void renderSticksOnly() {
        if (!hasSetupBallsAndSticks) {
            setUpBallsAndSticks();
        }

        groupBalls.setVisible(false);
        groupPaperChainSticks.setVisible(false);
        groupSticks.setVisible(true);
    }

    public void displayCpkModel() {
        newRequestToRenderCpkModel = true;
    }

    private void renderCpkModel() {
        if (!hasSetupBallsAndSticks) {
            setUpBallsAndSticks();
        }

        if (!hasResizedBalls) {
            setUpCpkModel();
        }

        groupBalls.setVisible(true);
        groupSticks.setVisible(false);
        groupPaperChainSticks.setVisible(false);
        hasResizedBalls = true;
    }

    public void displayPaperChain() {
        newRequestToRenderPaperChain = true;
    }

    public void renderPaperChain() {
        if (!hasSetupPaperChain) {
            setUpPaperChain();
        }

        if (hasResizedBalls) {
            resizeAllBalls(STANDARD_BALL_RADIUS);
            hasResizedBalls = false;
        }

        groupBalls.setVisible(true);
        groupSticks.setVisible(true);
        groupPaperChainSticks.setVisible(true);

    }

    private void setUpPaperChain() {

        new Thread(
                new Runnable() {
                    public void run() {

                        for (Molecule molecule : MainActivity.structure.getMolecules()) {
                            for (Ring ring : molecule.getRings()) {

                                if (ring.getNumberOfAtoms() == 5 || ring.getNumberOfAtoms() == 6) {// only for rings of 5 and 6 - > extend to 7
                                    ArrayList<Vector3d> p = new ArrayList<Vector3d>();
                                    for (Atom atom : ring.getAtoms()) {
                                        p.add(atom.getLocation());
                                    }
                                    Vector3d centroid = Vector3d.findCentroid(p);

                                    Number3D centroidNum3D = calculateShiftedPosition(centroid, originShift);

                                    for (int i = 0; i < ring.getNumberOfAtoms(); i++) {
                                        Stack<Number3D> points = new Stack<Number3D>();
                                        points.push(calculateShiftedPosition(ring.getAtomAt(i % ring.getNumberOfAtoms()).getLocation(), originShift));
                                        points.push(calculateShiftedPosition(ring.getAtomAt((i + 1) % ring.getNumberOfAtoms()).getLocation(), originShift));
                                        points.push(centroidNum3D);

                                        //PhongMaterial material = new PhongMaterial();
                                        SimpleMaterial material = new SimpleMaterial();
                                        Triangle triangle = new Triangle(points, Color.RED);
                                        triangle.setMaterial(material);
                                        triangle.setColor(ring.getRingColour());
                                        groupPaperChainSticks.addChild(triangle);

                                    }

                                }
                            }

                        }

                        groupPaperChainSticks.addLight(mLight);
                        groupPaperChainSticks.addLight(mLight2);

                        groupAll.addChild(groupPaperChainSticks);
                        hasSetupPaperChain = true;
                    }
                }).start();
    }

    /**
     * Uses a raster approach to filling a polygon with lines
     *
     * @param first
     * @param firstNeighbor
     * @param opposite
     * @param oppositesNeighbor
     * @param maxDepth
     * @param currentDepth
     * @param points
     * @deprecated
     */
    private void fillWithSticks(Number3D first, Number3D firstNeighbor, Number3D opposite, Number3D oppositesNeighbor, int maxDepth, int currentDepth, Stack<Number3D> points) {
        if (currentDepth <= maxDepth) {
            Number3D midpointFirstAndNeigh = getMidPoint(first, firstNeighbor);
            Number3D midpointOppositeAndNeigh = getMidPoint(opposite, oppositesNeighbor);

            points.push(midpointFirstAndNeigh);
            points.push(midpointOppositeAndNeigh);

            currentDepth++;
            fillWithSticks(first, midpointFirstAndNeigh, opposite, midpointOppositeAndNeigh, maxDepth, currentDepth, points);
            fillWithSticks(midpointFirstAndNeigh, firstNeighbor, midpointOppositeAndNeigh, oppositesNeighbor, maxDepth, currentDepth, points);
        } else {
            return;
        }

    }

    @Override
    public void onAsyncTaskComplete(Boolean... params) {
        displayBallAndStick();
    }

    public static class Renderer3DFactory implements SweetTouchRendererFactory<Renderer3D> {
        public Renderer3D factory(Context context) {
            return new Renderer3D(context);
        }
    }

    private class SetupBallsAndSticksTask extends AsyncTask<Void, Integer, Boolean>
    {
        private Context context;
        private TaskCallback<Boolean> callback = null;

        public SetupBallsAndSticksTask (Context context, TaskCallback<Boolean> callback)
        {
            this.context = context;
            this.callback = callback;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            if (MainActivity.structure == null) {
                return false;
            }

            Activity activity = (Activity) mContext;
            Canvas3D canvas = (Canvas3D) activity.getFragmentManager().findFragmentByTag(Canvas3D.FRAGMENT_TAG);
            canvas.showLoader();

            for (Molecule molecule : MainActivity.structure.getMolecules()) {

                for (Atom atom : molecule.getAtoms()) {

                    // SimpleMaterial material = new SimpleMaterial();
                    // DiffuseMaterial material = new DiffuseMaterial();
                    // SphereMapMaterial material = new SphereMapMaterial();
                    PhongMaterial material = new PhongMaterial();
                    // GouraudMaterial material = new GouraudMaterial();

                    if (originShift == null) {
                        findOriginShift(atom.getLocation());
                    }

                    AtomSphere ball = new AtomSphere(STANDARD_BALL_RADIUS, 9, 9, atom);
                    ball.setMaterial(material);
                    ball.getMaterial().setUseColor(true);
                    Number3D ballRenderPosition = calculateShiftedPosition(atom.getLocation(), originShift);
                    ball.setPosition(ballRenderPosition);
                    ball.setColor(PeriodicTable.getColour(atom));

                    groupBalls.addChild(ball);

                    for (Atom neighbour : atom.getNeighbours()) {

                        Number3D neighborPos = calculateShiftedPosition(neighbour.getLocation(), originShift);
                        Number3D midPoint = getMidPoint(ballRenderPosition, neighborPos);

                        Stack<Number3D> points = new Stack<Number3D>();
                        points.push(ballRenderPosition);
                        points.push(midPoint);

                        Line3D bond = new Line3D(points, STANDARD_LINE_THICKNESS, PeriodicTable.getColour(atom));
                        SimpleMaterial bondMaterial = new SimpleMaterial();
                        bond.setMaterial(bondMaterial);
                        bond.setColor(PeriodicTable.getColour(atom));
                        groupSticks.addChild(bond);

                        Stack<Number3D> points2 = new Stack<Number3D>();
                        points2.push(midPoint);
                        points2.push(neighborPos);

                        Line3D bond2 = new Line3D(points2, STANDARD_LINE_THICKNESS, PeriodicTable.getColour(neighbour));
                        SimpleMaterial bondMaterial2 = new SimpleMaterial();
                        bond2.setMaterial(bondMaterial2);
                        bond2.setColor(PeriodicTable.getColour(neighbour));
                        groupSticks.addChild(bond2);
                    }

                }
            }
            hasSetupBallsAndSticks = true;

            setUpGroups();
            addChild(groupAll);

            canvas.hideLoader();

            return hasSetupBallsAndSticks;
        }

        @Override
        protected void onPostExecute(Boolean success)
        {
            try {
                callback.onAsyncTaskComplete(success);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

}

