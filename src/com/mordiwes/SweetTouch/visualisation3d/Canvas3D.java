package com.mordiwes.SweetTouch.visualisation3d;

import com.mordiwes.SweetTouch.ui.MainActivity;
import com.mordiwes.SweetTouch.display.RajawaliFragmentCanvas;
import com.mordiwes.SweetTouch.display.SweetTouchRendererFactory;

/**
 * Created by Mordechai on 2014-09-18.
 */
public class Canvas3D extends RajawaliFragmentCanvas<Renderer3D> {//GestureDetector.OnGestureListener for lon press and others

    public static final String FRAGMENT_TAG = "Canvas3D";
    public static final String tag = "Canvas3D";

    public Canvas3D() {
        super();
    }

    public Canvas3D(SweetTouchRendererFactory<Renderer3D> rendererFactory, int fragmentLayoutResId) {
        super(rendererFactory, fragmentLayoutResId);
    }

    public void displayBallAndStickModel() {
        mRenderer.displayBallAndStick();
    }

    public void displayCpkModel()
    {
        mRenderer.displayCpkModel();
    }

    public void displayOnlyBallsAsPoints()
    {
        mRenderer.displayBallsOnlyAsPoints();
    }

    public void displayOnlySticks()
    {
        mRenderer.displaySticksOnly();
    }

    public void displayPaperChain()
    {
        mRenderer.displayPaperChain();
    }


}