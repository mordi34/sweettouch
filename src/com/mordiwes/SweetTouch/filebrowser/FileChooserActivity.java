package com.mordiwes.SweetTouch.filebrowser;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;
import com.mordiwes.SweetTouch.R;

import java.io.File;
import java.sql.Date;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Wesley on 7/20/2014.
 */
public class FileChooserActivity extends ListActivity {
    public static final String TAG = "FileChooserActivity";

    private File currentDir;
    private FileArrayAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Sweet Touch/");
        fill(currentDir);
    }

    private void fill(File f) {
        File[] dirs = f.listFiles();
        this.setTitle("Current Dir: " + f.getName());
        List<Item> dir = new ArrayList<Item>();
        List<Item> fls = new ArrayList<Item>();
        try {
            for (File ff : dirs) {
                Date lastModDate = new Date(ff.lastModified());
                DateFormat formatter = DateFormat.getDateTimeInstance();
                String date_modify = formatter.format(lastModDate);

                String fName = ff.getName();

                if (ff.isDirectory()) {
                    File[] fbuf = ff.listFiles();
                    int buf = 0;

                    if (fbuf != null) {
                        buf = fbuf.length;
                    }

                    Log.e(TAG, "before: " + buf);

                    for (File tempFile : fbuf) {
                        String tempFileName = tempFile.getName();
                        Log.e(TAG, tempFileName);
                        if (!tempFileName.substring(tempFileName.lastIndexOf(".") + 1).equalsIgnoreCase("pdb")) {
                            buf--;
                            Log.e(TAG, "" + buf);
                        }
                    }

                    String num_item = String.valueOf(buf);
                    if (buf == 1) {
                        num_item = num_item + " pdb file";
                    } else {
                        num_item = num_item + " pdb files";
                    }

                    dir.add(new Item(ff.getName(), num_item, date_modify, ff.getAbsolutePath(), "directory"));
                } else if (fName.substring(fName.lastIndexOf(".") + 1).equalsIgnoreCase("pdb")) { //Only displays pdb files
                    fls.add(new Item(ff.getName(), ff.length() + " Byte", date_modify, ff.getAbsolutePath(), "file"));
                }
            }
        } catch (Exception e) {

        }

        Collections.sort(dir);
        Collections.sort(fls);
        dir.addAll(fls);

        if (!f.getName().equalsIgnoreCase("")) {
            dir.add(0, new Item("..", "Parent Directory", "", f.getParent(), "directory_up"));
        }

        adapter = new FileArrayAdapter(FileChooserActivity.this, R.layout.file_browser, dir);
        this.setListAdapter(adapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Item o = adapter.getItem(position);
        if (o.getImage().equalsIgnoreCase("directory") || o.getImage().equalsIgnoreCase("directory_up")) {
            currentDir = new File(o.getPath());
            fill(currentDir);
        } else {
            onFileClick(o);
        }
    }

    private void onFileClick(Item o) {
        Toast.makeText(this, "File Selected: " + currentDir.toString() + "/" + o.getName(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent();
        intent.putExtra("GetPath", currentDir.toString());
        intent.putExtra("GetFileName", o.getName());
        intent.putExtra("FullPath", currentDir.toString() + "/" + o.getName());

        Log.v("FileChooser", "File selected: " + currentDir.toString() + "/" + o.getName());
        setResult(RESULT_OK, intent);
        finish();
    }
}

