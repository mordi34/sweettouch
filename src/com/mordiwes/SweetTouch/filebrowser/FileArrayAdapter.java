package com.mordiwes.SweetTouch.filebrowser;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.mordiwes.SweetTouch.R;

import java.util.List;

/**
 * Created by Wesley on 7/20/2014.
 */
public class FileArrayAdapter extends ArrayAdapter<Item> {

    private Context c;
    private int id;
    private List<Item> items;

    public FileArrayAdapter(Context context, int textViewResourceId,
                            List<Item> objects) {
        super(context, textViewResourceId, objects);
        c = context;
        id = textViewResourceId;
        items = objects;
    }

    public Item getItem(int i) {
        return items.get(i);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater vi = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(id, null);
        }

               /* create a new view of my layout and inflate it in the row */
        //convertView = ( RelativeLayout ) inflater.inflate( resource, null );

        final Item o = items.get(position);

        if (o != null) {
            TextView itemName = (TextView) view.findViewById(R.id.ItemName);
            TextView itemText = (TextView) view.findViewById(R.id.ItemText);
            TextView textViewDate = (TextView) view.findViewById(R.id.TextViewDate);
                       /* Take the ImageView from layout and set the city's imageResourceId */
            ImageView imageCity = (ImageView) view.findViewById(R.id.fd_Icon1);
            String uri = "drawable/" + o.getImage();
            int imageResource = c.getResources().getIdentifier(uri, null, c.getPackageName());
            Drawable image = c.getResources().getDrawable(imageResource);
            imageCity.setImageDrawable(image);

            if (itemName != null)
                itemName.setText(o.getName());
            if (itemText != null)
                itemText.setText(o.getData());
            if (textViewDate != null)
                textViewDate.setText(o.getDate());
        }
        return view;
    }
}

