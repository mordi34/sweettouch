package com.mordiwes.SweetTouch.async;

import java.io.IOException;

/**
 * Used by CommunuicationTask to send feedback to the class that called it.
 *
 * @author Mordechai on
 * @date 2014-10-01
 * @param <Params>
 *            The type of data to be sent back.
 */
public interface TaskCallback<Params> {
    void onAsyncTaskComplete(Params... params);
}
