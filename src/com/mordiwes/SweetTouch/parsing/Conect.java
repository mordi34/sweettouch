package com.mordiwes.SweetTouch.parsing;

import java.util.ArrayList;

/**
 * Created by Mordechai on 2014-10-01.
 */
public class Conect
{
    private int atomSerial;
    private ArrayList<Integer> connections = new ArrayList<Integer>();

    public void setAtomSerial(int serial)
    {
        atomSerial = serial;
    }

    public int getAtomSerial ()
    {
        return atomSerial;
    }

    public void addConnection (int serial)
    {
        connections.add(serial);
    }

    public ArrayList<Integer> getConnections ()
    {
        return connections;
    }

    public int getConnection (int number)
    {
        return connections.get(number);
    }
}
