package com.mordiwes.SweetTouch.parsing;

import android.content.Context;
import android.os.AsyncTask;
import com.mordiwes.SweetTouch.async.TaskCallback;
import com.mordiwes.SweetTouch.ui.DrawerPopulator;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Mordechai on 2014-10-01.
 */
public class DownloadFileTask extends AsyncTask<String,Void, Object[]> {

    private Context context;
    private TaskCallback<Object[]> callback = null;

    public DownloadFileTask (Context context, TaskCallback<Object[]> callback)
    {
        this.context = context;
        this.callback = callback;
    }

    @Override
    protected Object[] doInBackground(String... params) {
        int responseCode = 404;
        try {

           URL url = new URL(params[0]);

            FileOutputStream outputStream = context.openFileOutput(params[1], Context.MODE_PRIVATE);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            responseCode = conn.getResponseCode();

            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            String s = br.readLine(), line="";

            while (s != null) {
                line += s+"\n";
                s = br.readLine();
            }

            outputStream.write(line.getBytes());

            outputStream.close();
            br.close();
            conn.disconnect();

        } catch (Exception ex) {
            ex.printStackTrace();
            final File file = new File(context.getFilesDir(), params[1]);
            return new Object[] {responseCode, file};
        }

        final File file = new File(context.getFilesDir(), params[1]);
        return new Object[] {responseCode,file};
    }

    @Override
    protected void onPostExecute(Object[] result)
    {
        try {
            callback.onAsyncTaskComplete(result);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
