package com.mordiwes.SweetTouch.parsing;

import android.content.Context;
import android.os.AsyncTask;
import com.mordiwes.SweetTouch.async.TaskCallback;
import com.mordiwes.SweetTouch.elements.Atom;
import com.mordiwes.SweetTouch.elements.Structure;

import java.io.*;

/**
 * Created by Mordechai on 2014-10-08.
 */
public class PdbParseTask extends AsyncTask<Object,Void, Object[]> {

    private Context context;
    private TaskCallback<Object[]> callback = null;

    private PdbAtomLineParser atomLineParser = new PdbAtomLineParser();
    private PdbConectLineParser conectLineParser = new PdbConectLineParser();

    public PdbParseTask (Context context, TaskCallback<Object[]> callback)
    {
        this.context = context;
        this.callback = callback;
    }

    @Override
    protected Object[] doInBackground(Object... params) {
        //BufferedReader br = new BufferedReader(new FileReader(file));
        boolean containsConect = false;
        try {
            BufferedReader br = new BufferedReader(new FileReader((File)params[0]));

            String line = br.readLine();

        while (line != null) {
            String recordName = PdbLineParser.extractRecordNameFromLine(line);

            if (recordName.equals(PdbParser.ATOM)) {
                ((Structure)params[1]).addAtom(atomLineParser.parse(line));
            } else if (recordName.equals(PdbParser.HETATM)) {
                ((Structure)params[1]).addAtom(atomLineParser.parse(line));
            } else if (recordName.equals(PdbParser.LINK)) {
                //parse link
            }

            else if (recordName.equals(PdbParser.CONECT))
            {
                Conect conect = conectLineParser.parse(line);
                Atom atom =  ((Structure)params[1]).getCurrentMolecule().getAtomBySerial(conect.getAtomSerial());
                if (atom != null) {
                    for (Integer integer : conect.getConnections()) {
                        Atom neighbor =  ((Structure)params[1]).getCurrentMolecule().getAtomBySerial(integer);
                        if (neighbor != null) {
                            atom.addNeighbour(neighbor);
                        }


                    }
                }

                containsConect = true;
            }

            line = br.readLine();
        }

        br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Object [] ret = new Object[1];
        ret[0] = (Boolean)containsConect;

        return ret;
    }

    @Override
    protected void onPostExecute(Object[] result) {
        callback.onAsyncTaskComplete(result);
    }
}
