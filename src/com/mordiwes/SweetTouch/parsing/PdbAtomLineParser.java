package com.mordiwes.SweetTouch.parsing;

import com.mordiwes.SweetTouch.elements.Atom;
import com.mordiwes.SweetTouch.util.Vector3d;

/**
 * Treats ATOM and HETATM the same but records which it was
 * TODO error handling while parsing: try catch around all
 * Created by Mordechai on 2014-09-11.
 */
public class PdbAtomLineParser extends PdbLineParser<Atom> {

    public PdbAtomLineParser() {

    }

    @Override
    public Atom parse(String line) {
        if (!isValidLine(line))
            return null;

        Atom atom = new Atom();

        atom.setRecordType(extractRecordNameFromLine(line));

        atom.setSerial(extractSerial(line));

        atom.setName(extractAtomName(line));

        atom.setAlternateLocationIndicator(extractAlternateLocationIndicator(line));

        atom.setResidueName(extractResidueName(line));

        atom.setChainID(extractChainId(line));

        atom.setResidueSequenceNumber(extractResidueSequenceNumber(line));

        atom.setResidueInsertionCode(extractResidueInsertionCode(line));

        atom.setLocation(extractLocation(line));

        atom.setOccupancy(extractOccupancy(line));

        atom.setTempFactor(extractTemperatureFactor(line));

        atom.setElement(extractElement(line));

        atom.setCharge(extractCharge(line));

        return atom;
    }

    @Override
    protected boolean isValidLine(String line) {
        return extractRecordNameFromLine(line).equals(PdbParser.ATOM) || extractRecordNameFromLine(line).equals(PdbParser.HETATM);
    }

    private int extractSerial(String line) {
        String s = line.substring(6, 11).trim();
        int serial = Integer.parseInt(s);
        return serial;
    }

    private String extractAtomName(String line) {
        return line.substring(12, 16).trim();
    }

    private String extractAlternateLocationIndicator(String line) {
        return line.substring(16, 17);
    }

    private String extractResidueName(String line) {
        return line.substring(17, 20).trim();
    }

    private String extractChainId(String line) {
        return line.substring(21, 22);
    }

    private int extractResidueSequenceNumber(String line) {
        return Integer.parseInt(line.substring(22, 26).trim());
    }

    private String extractResidueInsertionCode(String line) {
        return line.substring(26, 27);
    }

    private Vector3d extractLocation(String line) {
        double x = Double.parseDouble(line.substring(30, 38).trim());
        double y = Double.parseDouble(line.substring(38, 46).trim());
        double z = Double.parseDouble(line.substring(46, 54).trim());

        return new Vector3d(x, y, z);

    }

    private double extractOccupancy(String line) {
        return Double.parseDouble(line.substring(54, 60).trim());
    }

    private double extractTemperatureFactor(String line) {
        return Double.parseDouble(line.substring(60, 66).trim());
    }

    private String extractElement(String line) {
        try {
            return line.substring(76, 78).trim();
        }catch (Exception e) {
            return " ";
        }
    }

    private String extractCharge(String line) {
        try {
            return line.substring(78, 79).trim();
        } catch (Exception e) {
            return " ";
        }

    }

}
