package com.mordiwes.SweetTouch.parsing;

/**
 * Created by Mordechai on 2014-10-01.
 */
public class PdbConectLineParser extends PdbLineParser<Conect> {


    @Override
    public Conect parse(String line) {
       if (!isValidLine(line))
           return null;

        Conect conect = new Conect();

        conect.setAtomSerial(extractAtomSerial(line));

        parseConnections(conect,line);

        return conect;

    }

    @Override
    protected boolean isValidLine(String line) {
        return extractRecordNameFromLine(line).equals(PdbParser.CONECT);
    }


    private int extractAtomSerial(String line) {
         String s = line.substring(6, 11).trim();
        int serial = Integer.parseInt(s);
        return serial;
    }

    private void parseConnections(Conect conect, String line)
    {
        int firstIndex = 11;
        int secondIndex = 16;
        for (int i = 0; i< 4; i++) {
            try {
                String s = line.substring(firstIndex, secondIndex).trim();
                int serial = Integer.parseInt(s);
                conect.addConnection(serial);
                firstIndex += 5;
                secondIndex += 5;
            }catch (Exception e)
            {
                break;
            }

        }

    }



}
