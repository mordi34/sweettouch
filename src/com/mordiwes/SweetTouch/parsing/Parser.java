package com.mordiwes.SweetTouch.parsing;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.mordiwes.SweetTouch.async.TaskCallback;
import com.mordiwes.SweetTouch.elements.Structure;
import com.mordiwes.SweetTouch.ui.MainActivity;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * Created by Mordechai on 2014-09-10.
 */
public abstract class Parser implements TaskCallback<Object[]> {
    protected File file;
    protected Structure structure;
    protected Context context;

    public Parser(Context context) {
        this.context = context;
    }

    public Parser(File file) {
        setFile(file);
    }


    public abstract void parse() throws Exception;

    public void parse(File file) throws Exception {
        setFile(file);
        parse();
    }

    public void parse(String file) throws Exception {
        setFile(file);
        parse();
    }

    public void parseUrl(String url, String moleculeName) throws Exception {
        loadFileFromUrl(url, moleculeName + ".pdb");

    }

    public void setFile(File file) {
        this.file = file;
        structure = new Structure(file);
    }

    private void loadFileFromUrl(final String urlString, final String fileName) throws Exception {

        final File file = new File(context.getFilesDir(), fileName);
        file.createNewFile();

        Toast.makeText( context,"Searching online... " , Toast.LENGTH_SHORT).show();
        DownloadFileTask dft = new DownloadFileTask(context,this);
        dft.execute(new String[]{urlString,fileName});

    }

    @Override
    public void onAsyncTaskComplete(Object[]... result) {

        if(((Integer)result[0][0]).equals(202) || ((Integer)result[0][0]).equals(200)) {
            System.out.println("Set file to :" + ((File) result[0][1]));
            setFile((File)result[0][1]);
            try {
                parse();
            }
            catch (OutOfMemoryError e) {
                Intent mStartActivity = new Intent(MainActivity.getContext(), MainActivity.class);
                int mPendingIntentId = 123456;
                PendingIntent mPendingIntent = PendingIntent.getActivity(MainActivity.getContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager mgr = (AlarmManager) MainActivity.getContext().getSystemService(Context.ALARM_SERVICE);
                mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
                System.exit(0);
            }
            catch (Exception e) {
                Toast.makeText(MainActivity.getContext(), "Unable to parse PDB file", Toast.LENGTH_LONG);
            }

            MainActivity.resetCanvas();
        } else {
            Toast.makeText( context,"Could not find molecule" , Toast.LENGTH_SHORT).show();
            //delete failed files
            ((File)result[0][1]).delete();
        }

    }

    public void setFile(String file) {
        setFile(new File(file));
    }

    public String getFileName()
    {
        return file.getName();
    }

    public String getFilePath()
    {
        return file.getAbsolutePath();
    }

    public Structure getStructure() {
        return structure;
    }

}
