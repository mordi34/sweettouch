package com.mordiwes.SweetTouch.parsing;

import android.content.Context;

import java.io.*;

/**
 * Created by Mordechai on 2014-09-10.
 */
public class PdbParser extends Parser {

    public static final String ATOM = "ATOM";
    public static final String HETATM = "HETATM";
    public static final String LINK = "LINK";
    public static final String CONECT = "CONECT";

    private boolean containsConect = false;

    private PdbAtomLineParser atomLineParser = new PdbAtomLineParser();
    private PdbConectLineParser connectLineParser = new PdbConectLineParser();

    public PdbParser(Context context) {
        super(context);
    }


    @Override
    public void parse() throws IOException, OutOfMemoryError {
//        BufferedReader br = new BufferedReader(new FileReader(file));

     BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));

        containsConect = false;

        String line = br.readLine();

        while (line != null) {
            String recordName = PdbLineParser.extractRecordNameFromLine(line);

            if (recordName.equals(ATOM)) {
                structure.addAtom(atomLineParser.parse(line));
            } else if (recordName.equals(HETATM)) {
                structure.addAtom(atomLineParser.parse(line));
            } else if (recordName.equals(LINK)) {
                //parse link
            }

            line = br.readLine();
        }

        br.close();

        // preprocessing
        //avoid preprocessing on empty structures
        if (structure.numberOfMolecules() > 0) {
            if (structure.getCurrentMolecule().getNumAtoms() > 0) {
                if(!containsConect) {
                    structure.computeMolecularGraphs();
                }

                structure.detectRings(true, true, true);
            }
        }


    }
}
