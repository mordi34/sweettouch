package com.mordiwes.SweetTouch.elements;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.mordiwes.SweetTouch.ui.MainActivity;
import com.mordiwes.SweetTouch.util.Constants;
import com.mordiwes.SweetTouch.util.ResidueTable;
import com.mordiwes.SweetTouch.util.Vector3d;
import rajawali.bounds.BoundingBox;
import rajawali.math.Number3D;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Wesley on 9/10/2014.
 */
public class Ring {
    public static String TAG = "Ring";
    // Defined by molecule.java I think?
    int ringID;
    List<Atom> atoms = new ArrayList<Atom>(); // Up to 6..
    List<Double> puckeringAngles = new ArrayList<Double>();
    List<RingLinkage> ringLinkages = new ArrayList<RingLinkage>();
    List<Ring> neighbourRings = new ArrayList<Ring>();
    private int ringColour;
    private Constants.RingColouring.RingType ringType;

    String residueName;
    List<String> stereocode = new ArrayList<String>();
    List<Double> dihedralAngles = new ArrayList<Double>();

    private boolean oriented;
    private boolean selected = true;
    private boolean hasComputedPuckeringAngles = false;
    private boolean cannotComputePuckeringAngles = false;
    private boolean hasFoundRingColour = false;

    public boolean visited = false;
    public boolean positioned = false;
    public Number3D displayPos2D;

    //May not be a useful constructor
    public Ring(int ringID) {
        this.ringID = ringID;
    }

    public Ring(int ringID, List<Atom> atoms) {
        if (atoms.size() <= 6) {
            this.ringID = ringID;
            this.atoms = atoms;
        } else {
            //TODO
        }
    }

    /**
     * Adds an atom to a ring after creation of the ring
     *
     * @param atom The atom to be added
     */
    public void addAtom(Atom atom) throws OutOfMemoryError{
        // Should this create links between atoms? I'm not sure this method will be useful, since creation of the ring should create the whole ring
        atoms.add(atom);

    }

    public boolean containsAtom(Atom atom) {
        return atoms.contains(atom);
    }

    /**
     * Reorders the atoms in the ring so that they are stored in the ArrayList in terms of their given (from the pdb)
     * position in the ring.
     */
    public void orientRing() {
        Atom oxygen = null;

        for (Atom atom : atoms) {
            if (atom.getElement().equals("O") || atom.getName().equals("O")) {
                oxygen = atom;
                break;
            }
        }

        if (oxygen == null || oxygen.getNeighbours().size() < 2) {
            return;
        }

        int carbonCount = 0;

        for (Atom child : oxygen.getNeighbours()) {
            carbonCount = (child.getElement().equals("C") || child.getName().equals("C")) ? ++carbonCount : carbonCount; //Increment counter for every carbon
        }

        if (carbonCount < 2) {
            return;
        }

        atoms.remove(oxygen);

        //TODO: Better sort
        for (int i = 0; i < atoms.size(); i++) {
            for (int j = atoms.size() - 1; j > i; j--) {
                if (atoms.get(j).getRingPosition() < atoms.get(j - 1).getRingPosition()) {
                    Collections.swap(atoms, j, j - 1);
                }
            }
        }

        atoms.add(oxygen);
        this.oriented = true;
    }

    /**
     * Creates stereocode of ring for residue name lookup
     *
     * @return true if successful, false if not
     */
    public boolean constructStereocode() throws OutOfMemoryError {
        Atom previousAtom;
        Atom currentAtom;
        Atom nextAtom;
        Atom substituentAtom1 = null;
        Atom substituentAtom2 = null;
        Atom tempAtom;

        // Prevent stereocodes for non-carbohydrate rings
        if (!this.oriented) return false;

        stereocode.add(atoms.size() + "");

        try {
            for (int i = atoms.size() - 2; i >= 0; i--) {
                currentAtom = atoms.get(i);

                if (i - 1 < 0) {
                    previousAtom = atoms.get(atoms.size() - 1);
                } else {
                    previousAtom = atoms.get((i - 1) % atoms.size());
                }

                nextAtom = atoms.get((i + 1) % atoms.size());   //Wrap if i+1 == atoms.size()

                for (Atom child : currentAtom.getNeighbours()) {
                    if (child.getContainingRingID() == -1 && !child.equals(substituentAtom1) && substituentAtom1 == null) {
                        substituentAtom1 = child;
                    }
                    if (child.getContainingRingID() == -1 && !child.equals(substituentAtom2) && substituentAtom2 == null && !child.equals(substituentAtom1)) {
                        substituentAtom2 = child;
                    }
                }

                if (substituentAtom1 != null && substituentAtom2 != null) {
                    if ((substituentAtom1.getElement().equals("H") || substituentAtom1.getName().equals("H")) &&
                            ((substituentAtom2.getElement().equals("O") || substituentAtom2.getName().equals("O")) ||
                                    (substituentAtom2.getElement().equals("N") || substituentAtom2.getName().equals("N")) ||
                                    (substituentAtom2.getElement().equals("C") || substituentAtom2.getName().equals("C")))) {
                        tempAtom = substituentAtom2;
                        substituentAtom2 = substituentAtom1;
                        substituentAtom1 = tempAtom;
                    }

                    if ((substituentAtom1.getElement().equals("H") || substituentAtom1.getName().equals("H")) && (substituentAtom2.getElement().equals("H") || substituentAtom2.getName().equals("H"))) {
                        stereocode.add("_");
                    } else {
                        double angle = dihedral(previousAtom, substituentAtom1, substituentAtom2, nextAtom);

                        dihedralAngles.add(angle);

                        if (angle < 0) {   //Negative means substituent is upward
                            stereocode.add("1");
                        } else {
                            stereocode.add("2");
                        }
                    }
                    substituentAtom1 = null;
                    substituentAtom2 = null;
                }
            }

            getResidueNameFromDatabase();
            if (residueName == null) {
                Log.w(TAG, "Stereocode not found in database.");
            }
        } catch (OutOfMemoryError e) {
            throw e;
        } catch(Exception e) {
            Log.e(TAG, "Failed to construct stereocode for residue name lookup", e);
            return false;
        }

        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ring)) return false;

        Ring ring = (Ring) o;

        return ringID == ring.getRingID();
    }

    public int getNumberOfAtoms() {
        return atoms.size();
    }

    public Atom getAtomAt(int index) {
        return atoms.get(index);
    }

    @Override
    public String toString() {
        String atomList = "";
        for (Atom atom : atoms) {
            atomList += atom.getSerial() + ", ";
//            atomList += atom.toString() + "\n";
        }
        String linkList = "";
        for (RingLinkage ringLinkage : ringLinkages) {
            linkList += ringLinkage.getRingLinkageID() + ringLinkage.getOtherRing().getResidueName() + ", ";
//            linkList += ringLinkage.toString() + "\n";
        }

        StringBuilder stereocodeStr = new StringBuilder();
        for (String word : stereocode) {
            stereocodeStr.append(word);
        }
        if (isOriented()) {
            return "Ring{" +
                    "ringLinkageID=" + ringID +
                    ", atoms=" + atomList.substring(0, atomList.length() - 2) +
                    ", residueName='" + residueName + '\'' +
                    ", stereocode='" + stereocodeStr + '\'' +
                    ", ringLinkages=" + linkList.substring(0, linkList.length() - 2) +
                    ", isOriented=" + oriented +
                    ", isSelected=" + selected +
                    '}';
        } else {
            return "Ring{" +
                    "ringLinkageID=" + ringID +
                    ", atoms=" + atomList.substring(0, atomList.length() - 2) +
                    //", residueName='" + residueName + '\'' +
                    //", stereocode='" + stereocodeStr + '\'' +
//                    ", ringLinkages=" + linkList.substring(0, linkList.length() - 2) +
                    ", isOriented=" + oriented +
                    ", isSelected=" + selected +
                    '}';
        }
    }

    /**
     * ****************
     * Private methods *
     * *****************
     */

    private double dihedral(Atom a1, Atom a2, Atom a3, Atom a4) {
        Vector3d b1 = Vector3d.subtract(a2.getLocation(), a1.getLocation());
        Vector3d b2 = Vector3d.subtract(a3.getLocation(), a2.getLocation());
        Vector3d b3 = Vector3d.subtract(a4.getLocation(), a3.getLocation());

        Vector3d n1 = Vector3d.crossProduct(b1, b2);
        Vector3d n2 = Vector3d.crossProduct(b2, b3);

        double psin = Vector3d.dotProduct(n1, b3) * b2.level();
        double pcos = Vector3d.dotProduct(n1, n2);

        return Math.atan2(psin, pcos) * (180.0 / Math.PI);
    }

    private void getResidueNameFromDatabase() throws NullPointerException {
        StringBuilder stereocodeStr = new StringBuilder();

        for (int i = 0 ; i < stereocode.size()-1 ; i++) {
            stereocodeStr.append(stereocode.get(i));
        }

        residueName = ResidueTable.getResidueName(stereocodeStr.toString());

        if (residueName == null) {
            residueName = atoms.size() == 5 ? ResidueTable.UNKNOWN5 : ResidueTable.UNKNOWN6;
        }
    }

    /**
     * Adapted from Dennie's Validator
     */
    private void computePuckeringAngles() {
        if (getNumberOfAtoms() < 5) {
            return;
        }

        Atom prev = null;
        Atom cur = null;
        Atom next = null;

        Vector3d n = null;
        Vector3d r0 = null;
        Vector3d r1 = null;
        Vector3d a = null;
        Vector3d a0 = null;
        Vector3d a1 = null;
        Vector3d p = null;
        Vector3d q = null;
        Double angle;

        //only calculate puckering for oriented rings
        if (!this.oriented)
            Log.d(TAG, "Unoriented ring puckering found");

        a0 = atoms.get(2).getLocation().subtract(atoms.get(0).getLocation());
        a1 = atoms.get(4).getLocation().subtract(atoms.get(2).getLocation());

        n = a0.crossProduct(a1);

        //process rest of the ring
        for (int i = 0; i < atoms.size() - 3; i++) {
            prev = atoms.get((2 * i) % atoms.size());
            cur = atoms.get((2 * i + 1) % atoms.size());
            next = atoms.get((2 * i + 2) % atoms.size());

            a = prev.getLocation().subtract(next.getLocation());
            r0 = cur.getLocation().subtract(prev.getLocation());
            r1 = next.getLocation().subtract(cur.getLocation());
            p = r0.crossProduct(r1);
            q = a.crossProduct(p);

            angle = 90 - (Math.acos((q.dotProduct(n)) / (Math.sqrt(q.dotProduct(q)) * Math.sqrt(n.dotProduct(n))))) * (180.0 / Math.PI);
            puckeringAngles.add(angle);
        }

        hasComputedPuckeringAngles = true;
    }


    //adapted from VMD
    private void computeRingColour() {

        if (getNumberOfAtoms() == 5 || getNumberOfAtoms() == 6) {

            boolean lighten = false;

            if (getNumberOfAtoms() == 6) {
                int pos = Constants.RingColouring.sixRingPosition(puckeringAngles);
                if (pos < 0) {
                    lighten = true;
                    pos = pos * -1;
                }

                //Map to colour
                if (pos == 0) {                //Planar
                    ringType = Constants.RingColouring.RingType.PLANAR;
                } else if (pos <= 2) {        //Chair
                    ringType = Constants.RingColouring.RingType.CHAIR;
                } else if (pos <= 8) {        //Boat
                    ringType = Constants.RingColouring.RingType.BOAT;
                } else if (pos <= 20) {        //Half-Chair
                    ringType = Constants.RingColouring.RingType.HALF_CHAIR;
                } else if (pos <= 26) {        //Skew-Boat
                    ringType = Constants.RingColouring.RingType.SKEW_BOAT;
                } else if (pos <= 38) {        //Envelope
                    ringType = Constants.RingColouring.RingType.RING6_ENVELOPE;
                }

            }

            if (getNumberOfAtoms() == 5) {
                int pos = Constants.RingColouring.fiveRingPosition(puckeringAngles);

                if (pos < 0) {
                    lighten = true;
                    pos = pos * -1;
                }

                //Map to colour
                if (pos == 0) {                //Planar
                    ringType = Constants.RingColouring.RingType.PLANAR;
                } else if (pos <= 10) {        //Twist
                    ringType = Constants.RingColouring.RingType.RING5_TWIST;
                } else if (pos <= 20) {        //Envelope
                    ringType = Constants.RingColouring.RingType.RING5_ENVELOPE;
                }
            }

            //if lighten = true no close match was found and colour should be lightened
            if (lighten) {
                ringColour = ringType.getLightenedColour();
            } else {
                ringColour = ringType.getColour();
            }

        }//end 5 and 6
        else if (getNumberOfAtoms() == 7) //Brown
        {
            ringType = Constants.RingColouring.RingType.RING7;
            ringColour = ringType.getColour();
        } else //Default - Grey
        {
            ringType = Constants.RingColouring.RingType.DEFAULT;
            ringColour = ringType.getColour();
        }

        Log.d(TAG, "Ring of type :" + ringType.toString());
        hasFoundRingColour = true;
    }

    public void findRingColour() {
        if (cannotComputePuckeringAngles) {
            return;
        }

        if (hasFoundRingColour) {
            return;
        }

        if (!hasComputedPuckeringAngles) {
            computePuckeringAngles();
            if (hasComputedPuckeringAngles) {
                computeRingColour();
            } else {
                cannotComputePuckeringAngles = true;
            }

        } else {
            computeRingColour();
        }

    }


    /**
     * ********************
     * Getters and Setters *
     * ********************
     */

    public int getRingColour() {
        return ringColour;
    }

    public int getRingID() {
        return ringID;
    }

    public List<Atom> getAtoms() {
        return atoms;
    }

    public void setAtoms(List<Atom> atoms) {
        this.atoms = atoms;
    }

    public String getResidueName() {
        return residueName == null ? "" : residueName;
    }

    public void setResidueName(String residueName) {
        this.residueName = residueName;
    }

    public boolean isOriented() {
        return oriented;
    }

    public void addRingLinkage(RingLinkage ringLinkage) throws OutOfMemoryError {
        if (!ringLinkages.contains(ringLinkage)) {
            ringLinkages.add(ringLinkage);
        }

        Ring neighbour = ringLinkage.getOtherRing();
        if (!neighbourRings.contains(neighbour)) {
            neighbourRings.add(neighbour);
        }
    }

    public List<RingLinkage> getRingLinkages() {
        return ringLinkages;
    }

    public void addNeighbourRing(Ring neighbourRing) {
        neighbourRings.add(neighbourRing);
    }

    public List<Ring> getNeighbourRings() {
        return neighbourRings;
    }

    public void setRingLinkages(List<RingLinkage> ringLinkages) {
        this.ringLinkages = ringLinkages;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Number3D getDisplayPos2D() {
        return displayPos2D;
    }

    public void setDisplayPos2D(Number3D displayPos2D) {
        this.displayPos2D = displayPos2D;
    }

    public void setDisplayPos2D(float xPos, float yPos) {
        Number3D temp = new Number3D(xPos, yPos, 0f);
        setDisplayPos2D(temp);
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public boolean isPositioned() {
        return positioned;
    }

    public void setPositioned(boolean positioned) {
        this.positioned = positioned;
    }

    public void clear() {
        for (RingLinkage ringLinkage : ringLinkages) {
            ringLinkage.clear();
        }
        ringLinkages.clear();
    }
}
