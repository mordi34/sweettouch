package com.mordiwes.SweetTouch.elements;

import android.util.Log;
import com.mordiwes.SweetTouch.util.PeriodicTable;
import com.mordiwes.SweetTouch.util.Vector3d;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Just a skeleton for use by the parser
 * Created by Mordechai on 2014-09-11.
 */
public class Molecule {
    private List<Atom> atoms = new ArrayList<Atom>();
    private List<Ring> rings = new ArrayList<Ring>();

    private boolean selected = true;

    public void addAtom(Atom atom) throws OutOfMemoryError {
        atoms.add(atom);
    }

    /**
     * Detects cycles and identifies relevant cycles as rings, adding them to the rings ArrayList
     */
    public void detectRings() throws OutOfMemoryError {
        //http://pubs.cs.uct.ac.za/archive/00000543/01/KuttelManuscript.pdf
        Stack<Atom> atomStack = new Stack<Atom>();
        setAllAtomsUnvisited();
        Atom initial = atoms.get(0);

        //TODO: Better name than findBackEdges?
        findBackEdges(atomStack, initial);

        atomStack.clear();

        //Checks that all atoms have been considered
        for (Atom currentAtom : atoms) {
            if (!currentAtom.isVisited()) {
                findBackEdges(atomStack, currentAtom);
            }
        }

    }

    /**
     * Detects links between rings, storing links in each ring class
     */
    public void detectRingLinkages() throws OutOfMemoryError {
        Stack<Atom> atomStack = new Stack<Atom>();
        setAllAtomsUnvisited();
        int currentAtomRingPos;
        int childAtomRingPos;

        int linkIDCounter = 0;

        //TODO: Make this look a little more like detectRings..

        for (Ring ring : rings) {
            if (ring.isOriented()) {    //Prevents searching for non-carbohydrate rings
                for (Atom ringAtom : ring.atoms) {
                    ringAtom.setParent(ringAtom);
                    atomStack.push(ringAtom);

                    while (atomStack.size() > 0) {
                        Atom currentAtom = atomStack.pop();
                        currentAtom.setVisited();
                        Atom parentAtom = currentAtom.getParent();

                        for (Atom childAtom : currentAtom.getNeighbours()) {
                            //Child must not be a part of the current ring, not be a parent of currentAtom, and not be visited
                            if (!ring.containsAtom(childAtom) &&
                                    !childAtom.equals(parentAtom) &&
                                    !childAtom.isVisited()) {
                                //Child must be a part of (any) another oriented ring
                                if (childAtom.getContainingRingID() != -1) {
                                    Ring childRing = rings.get(childAtom.getContainingRingID());

                                    if (childRing.isOriented()) {
                                        currentAtomRingPos = ringAtom.getRingPosition();
                                        childAtomRingPos = childAtom.getRingPosition();

                                        if (currentAtomRingPos == ring.getAtoms().size() - 1) {
                                            currentAtomRingPos = ring.getAtoms().size();
                                        }
                                        if (childAtomRingPos == ring.getAtoms().size() - 1) {
                                            childAtomRingPos = childRing.getAtoms().size();
                                        }

                                        if (!ring.getNeighbourRings().contains(childRing)) {

                                            ring.addRingLinkage(new RingLinkage(currentAtomRingPos, childAtomRingPos, childRing, linkIDCounter));
                                            childRing.addRingLinkage(new RingLinkage(childAtomRingPos, currentAtomRingPos, ring, linkIDCounter));

                                            linkIDCounter++;
                                        }
                                    }
                                } else {
                                    childAtom.setParent(currentAtom);
                                    atomStack.push(childAtom);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Creates residue names and assigns them to rings
     */
    public void determineResidueName() throws OutOfMemoryError {
        for (Ring ring : rings) {
            ring.constructStereocode();
        }
    }

    public void constructMolecularGraph() {

        Atom currentAtom = null;
        float currentAtomVdwRadius = 0f;

        Atom checkAtom = null;
        float checkAtomVdwRadius = 0f;

        for (int i = 0; i < atoms.size(); i++) {
            try {
                currentAtom = atoms.get(i);
                currentAtomVdwRadius = PeriodicTable.getVanDeWaalRadius(currentAtom);

                for (int j = i + 1; j < atoms.size(); j++) {
                    checkAtom = atoms.get(j);
                    checkAtomVdwRadius = PeriodicTable.getVanDeWaalRadius(checkAtom);

                    Vector3d distance = new Vector3d(Math.abs(checkAtom.getLocation().getX() - currentAtom.getLocation().getX()),
                            Math.abs(checkAtom.getLocation().getY() - currentAtom.getLocation().getY()),
                            Math.abs(checkAtom.getLocation().getZ() - currentAtom.getLocation().getZ()));

                    double distanceSumSquared = (distance.getX() * distance.getX()) + (distance.getY() * distance.getY()) + (distance.getZ() * distance.getZ());
                    float radiusSum= (currentAtomVdwRadius + checkAtomVdwRadius);
                    double cut = radiusSum * 0.55; //why does kieren have this?
                    cut *= cut;

                    if (distanceSumSquared < cut) {
                        checkAtom.addNeighbour(currentAtom);
                        currentAtom.addNeighbour(checkAtom);
                    }
                }
            } catch (Exception e) {
                Log.d("Molecule", "Didn't add atom to graph");
                //don't load at all in this case?
            }
        }
    }

    public void findRingColours() {
        for (Ring ring : rings) {
            ring.findRingColour();
        }
    }

    @Override
    public String toString() {
        String atomList = "";
        for (Atom atom : atoms) {
            atomList += atom.getSerial() + ", ";
//            atomList += atom.toString() + "\n";
        }
        String ringList = "";
        for (Ring ring : rings) {
            ringList += ring.getRingID() + ", ";
//            ringList += ring.toString() + "\n";
        }
        return "Molecule{" +
                "atoms=" + atomList.substring(0, atomList.length() - 2) + "\n" +
                "rings=" + ringList.substring(0, ringList.length() - 2) + "\n" +
                ", isSelected=" + selected +
                '}';
    }

    /**
     * ********************
     * Getters and Setters *
     * *********************
     */

    public List<Atom> getAtoms() {
        return atoms;
    }

    /**
     * Return Atom of the given serial number or null if none found.
     *
     * @param serial serial number of Atom to return
     * @return Atom of the given serial number
     */
    public Atom getAtomBySerial(int serial) {
        for (Atom atom : atoms) {
            if (atom.getSerial() == serial) {
                return atom;
            }
        }

        return null;
    }

    public int getNumAtoms() {
        return atoms.size();
    }

    public void setAtoms(List<Atom> atoms) {
        this.atoms = atoms;
    }

    public List<Ring> getRings() {
        return rings;
    }

    public void setRings(List<Ring> rings) {
        this.rings = rings;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    /*******************
     * Private methods *
     *******************/

    /**
     * Sets the visited boolean of all atoms in the molecule to false
     */
    private void setAllAtomsUnvisited() {
        for (Atom atom : atoms) {
            atom.setUnvisited();
        }
    }

    /**
     * Finds back-edges which denote cycles in the molecular graph
     *
     * @param atomStack
     * @param initial
     */
    private void findBackEdges(Stack<Atom> atomStack, Atom initial) throws OutOfMemoryError{
        initial.setParent(initial);
        atomStack.push(initial);

        while (atomStack.size() > 0) {
            Atom currentAtom = atomStack.pop();
            currentAtom.setVisited();

            Atom parentAtom = currentAtom.getParent();

//            Log.d("FindBackEdges", "CurrentAtom: " + currentAtom.getSerial());

            for (Atom neighbour : currentAtom.getNeighbours()) {
                Atom childAtom = neighbour;
                if (!childAtom.equals(parentAtom)) {
//                    Log.d("FindBackEdges", "ChildAtom: " + childAtom.getSerial());
                    if (childAtom.isVisited()) {
                        //Back edge found
                        boolean currentAtomInRing = false;
                        for (Ring ring : rings) {
                            if (ring.containsAtom(currentAtom)) {
                                currentAtomInRing = true;
                                break;
                            }
                        }

                        if (!currentAtomInRing) {
                            backTrace(childAtom, currentAtom);
                        }
                    } else {
                        //Process child
                        childAtom.setParent(currentAtom);
                        atomStack.push(childAtom);
                    }
                }
            }
        }
    }

    /**
     * Creates a new ring from the backedges found, and confirms that the new ring is indeed a ring (contained atoms
     * must be less than 8) and not just a cyclic structure
     *
     * @param childAtom
     * @param currentAtom
     */
    private void backTrace(Atom childAtom, Atom currentAtom) throws OutOfMemoryError{
        int newRingID = rings.size();
        Ring ring = new Ring(newRingID);
        ring.addAtom(childAtom);
        ring.addAtom(currentAtom);
        Atom parent = currentAtom.getParent();

        while (!parent.equals(childAtom)) {
            ring.addAtom(parent);
            parent = parent.getParent();
        }

        if (ring.getAtoms().size() < 8) {
            // This is a ring, not a cyclic molecule
            for (Atom atom : ring.getAtoms()) {
                atom.setContainingRingID(newRingID);
            }

            ring.orientRing();

            rings.add(ring);
        }
    }

    public void clear () {
        for (Atom atom : atoms) {
            if (atom != null) {
                atom.clear();
            }
        }
        atoms.clear();

        for (Ring ring : rings) {
            if (ring != null) {
                ring.clear();
            }
        }
        rings.clear();

    }
}
