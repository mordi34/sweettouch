package com.mordiwes.SweetTouch.elements;

import android.util.Log;
import com.mordiwes.SweetTouch.util.Vector3d;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wesley on 9/10/2014.
 */
public class Atom {
    // Attributes from PDB
    private String recordType;
    private int serial;
    private String name;
    private String alternateLocationIndicator;
    private String residueName;
    private String chainID;
    private int residueSequenceNumber;// is integer in PDB
    private String residueInsertionCode;
    private Vector3d location;
    private double occupancy;
    private double tempFactor;
    private String element;
    private String charge;

    // Ring finding
    private boolean visited = false;    // Initially false for all atoms, this is just used in ring finding
    private Atom parent;    // This could maybe be substituted with an int, which would store the parent serialID
    private int containingRingID = -1;
    private String ringPosition = ""; //For atoms contained in rings, eg "C1"

    // Molecular structure
    private List<Atom> neighbours = new ArrayList<Atom>();  // Similar to parent, this could maybe be substituted with
    // just the serial nums of neighbouring atoms, however I'm
    // less inclined to think it's a good idea here.

    private boolean selected = true;

    public Atom() {
    }



    // Add other constructors as necessary

//    public Atom (double x, double y, double z) {
//        neighbours = new ArrayList<Atom>();
//        this.location = new Vector3d(x, y, z);
//    }

    /******************
     * Public methods *
     ******************/

    /**
     * Adds a neighbour to the list of neighbours to this atom
     *
     * @param neighbor
     */
    public void addNeighbour(Atom neighbor) {
        //TODO: Probably something to prevent adding to an uninitialised neighbours list if (length != 0) or something
        if (!neighbours.contains(neighbor)) {
            neighbours.add(neighbor);
        }

    }

    @Override
    public boolean equals(Object rhs) {
        //TODO: Rethink/extend (Should this not throw an exception if rhs is not an instanceof Atom?)
//        if (this == rhs) return true;

        if (rhs == null) return false;

        if (rhs instanceof Atom) {
            Atom otherAtom = (Atom) rhs;

            if (otherAtom.getSerial() != this.getSerial()) return false;
            else if (!otherAtom.getLocation().equals(this.getLocation())) return false;

            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Atom{" +
                "recordType='" + recordType + '\'' +
                ", serial=" + serial +
                ", name='" + name + '\'' +
                ", alternateLocationIndicator='" + alternateLocationIndicator + '\'' +
                ", residueName='" + residueName + '\'' +
                ", chainID='" + chainID + '\'' +
                ", residueSequenceNumber=" + residueSequenceNumber +
                ", residueInsertionCode='" + residueInsertionCode + '\'' +
                ", location=" + location +
                ", occupancy=" + occupancy +
                ", tempFactor=" + tempFactor +
                ", element='" + element + '\'' +
                ", charge='" + charge + '\'' +
                ", containingRingID=" + containingRingID +
                ", neighbours=" + neighboursToString() +
                ", isSelected=" + selected +
                '}';
    }

    public String neighboursToString() {
        String toString = "";
        for (Atom atom : neighbours) {
            toString += atom.getName() + ", ";
        }

        return toString;
    }

    /**
     * ********************
     * Getters and Setters *
     * *********************
     */

    public Vector3d getLocation() {
        return location;
    }

    public void setLocation(Vector3d location) {
        this.location = location;
    }

    public String getName() {
        return getAtomSymbol();//substring and shit
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }

    public int getResidueSequenceNumber() {
        return residueSequenceNumber;
    }

    public void setResidueSequenceNumber(int residueSequenceNumber) {
        this.residueSequenceNumber = residueSequenceNumber;
    }

    public int getContainingRingID() {
        return containingRingID;
    }

    public void setContainingRingID(int containingRingID) {
        this.containingRingID = containingRingID;
    }

    public List<Atom> getNeighbours() {
        return neighbours;
    }

    public void setNeighbours(List<Atom> neighbours) {
        this.neighbours = neighbours;
    }

    public String getAlternateLocationIndicator() {
        return alternateLocationIndicator;
    }

    public void setAlternateLocationIndicator(String alternateLocationIndicator) {
        this.alternateLocationIndicator = alternateLocationIndicator;
    }

    public String getResidueName() {
        return residueName;
    }

    public void setResidueName(String residueName) {
        this.residueName = residueName;
    }

    public String getChainID() {
        return chainID;
    }

    public void setChainID(String chainID) {
        this.chainID = chainID;
    }

    public String getResidueInsertionCode() {
        return residueInsertionCode;
    }

    public void setResidueInsertionCode(String residueInsertionCode) {
        this.residueInsertionCode = residueInsertionCode;
    }

    public double getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(double occupancy) {
        this.occupancy = occupancy;
    }

    public double getTempFactor() {
        return tempFactor;
    }

    public void setTempFactor(double tempFactor) {
        this.tempFactor = tempFactor;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited() {
        this.visited = true;
    }

    public void setUnvisited() {
        this.visited = false;
    }

    public Atom getParent() {
        return parent;
    }

    public void setParent(Atom parent) {
        this.parent = parent;
    }

    //TODO: Check that this is necessary, what with getElement
    public String getAtomSymbol() {
        String symbol = name.replaceAll("[0-9]", "").charAt(0) + "";
        return symbol.trim();

    }

    public int getRingPosition() {
        String ringPos = name.replaceAll("[^0-9]", "");
        return ringPos.equals("") ? -1 : Integer.parseInt(ringPos);
    }

    public void setRingPosition(String ringPosition) {
        this.ringPosition = ringPosition;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public void clear() {
        neighbours.clear();
    }
}
