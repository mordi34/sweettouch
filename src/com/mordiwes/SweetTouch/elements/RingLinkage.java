package com.mordiwes.SweetTouch.elements;

import rajawali.math.Number3D;

import java.util.Stack;

/**
 * Created by Wesley on 9/18/2014.
 */
public class RingLinkage {
    private int ringLinkageID;
    private int linkExitPosition;   //Atom ring position in ring where link begins
    private int linkEnterPosition;  //Atom ring position in ring where link points
    private Ring otherRing;

    public boolean selected = true;
    public Stack<Number3D> displayPos2D = new Stack<Number3D>();    //FOR NOW, DON'T BE SCARED TO CHANGE MOFO

    public RingLinkage(int linkExitPosition, int linkEnterPosition, Ring otherRing, int linkID) {
        this.linkExitPosition = linkExitPosition;
        this.linkEnterPosition = linkEnterPosition;
        this.otherRing = otherRing;
        ringLinkageID = linkID;
    }

    @Override
    public boolean equals(Object otherRingLinkage) {
        if (otherRingLinkage instanceof RingLinkage) {
            RingLinkage newRingLinkage = (RingLinkage) otherRingLinkage;
            return this.getRingLinkageID() == newRingLinkage.getRingLinkageID();
        }
        return false;
    }

    @Override
    public String toString() {
        return "RingLinkage{" +
                "ringLinkageID=" + ringLinkageID +
                ", linkExitPosition=" + linkExitPosition +
                ", linkEnterPosition=" + linkEnterPosition +
                ", otherRing=" + otherRing +
                ", selected=" + selected +
                ", displayPos2D=" + displayPos2D +
                '}';
    }

    /***********************
     * Getters and Setters *
     * *********************/

    public int getRingLinkageID() {
        return ringLinkageID;
    }

    public int getLinkExitPosition() {
        return linkExitPosition;
    }

    public void setLinkExitPosition(int linkExitPosition) {
        this.linkExitPosition = linkExitPosition;
    }

    public int getLinkEnterPosition() {
        return linkEnterPosition;
    }

    public void setLinkEnterPosition(int linkEnterPosition) {
        this.linkEnterPosition = linkEnterPosition;
    }

    public Ring getOtherRing() {
        return otherRing;
    }

    public void setOtherRing(Ring otherRing) {
        this.otherRing = otherRing;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Stack<Number3D> getDisplayPos2D() {
        return displayPos2D;
    }

    public void addDisplayPos(Number3D position) {
        displayPos2D.push(position);
    }

    public void setDisplayPos2D(Stack<Number3D> displayPos2D) {
        this.displayPos2D = displayPos2D;
    }

    public void clear() {
        displayPos2D.clear();
    }
}
