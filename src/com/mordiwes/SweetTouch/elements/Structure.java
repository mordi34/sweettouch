package com.mordiwes.SweetTouch.elements;

import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Mordechai on 2014-09-11.
 */
public class Structure {
    private ArrayList<Molecule> molecules = new ArrayList<Molecule>();
    private int currentMolecule = 0;
    private final File fileLoadedFrom;

    public Structure(File fileLoadedFrom) {
        this.fileLoadedFrom  = fileLoadedFrom;
        addMolecule();// add initial molecule
    }

    public void addMolecule(Molecule mol) {
        molecules.add(mol);
    }

    public void addMolecule() {
        molecules.add(new Molecule());
    }

    public int numberOfMolecules() {
        return molecules.size();
    }

    public Molecule getMolecule(int number) {
        return molecules.get(number);
    }

    public ArrayList<Molecule> getMolecules() {
        return molecules;
    }

    public void addAtom(Atom atom) throws OutOfMemoryError {
        molecules.get(currentMolecule).addAtom(atom);
    }

    public void setCurrentMolecule(int moleculeNumber) {
        this.currentMolecule = moleculeNumber;
    }

    public Molecule getCurrentMolecule ()
    {
        return molecules.get(currentMolecule);
    }

    public void computeMolecularGraphs() {
        for (Molecule molecule : molecules) {// TODO put this async
            molecule.constructMolecularGraph();
        }
    }

    public void detectRings(boolean shouldDetectLinkages, boolean shouldDetermineResidueNames, boolean shouldColourRings) throws OutOfMemoryError {
      //  int countMol = 0;
        for (Molecule molecule : molecules) { // TODO: Put this async
            molecule.detectRings();
            if (shouldDetectLinkages) {
                molecule.detectRingLinkages();
            }
            if(shouldDetermineResidueNames) {
                molecule.determineResidueName();
            }



//            for (Ring ring : molecule.getRings()) {
//             //   Log.d("Rings", "[" + countMol + "] " + ring.toString());
//            }
            //countMol++;
        }

        for (Molecule molecule : molecules)
        {
            if(shouldColourRings)
            {
                molecule.findRingColours();
            }
        }

    }

    public String getFileLoadedFromName ()
    {
        return fileLoadedFrom.getName();
    }

    public void clear() {
        for (Molecule molecule : molecules) {
            if (molecule != null) {
                molecule.clear();
            }
        }
        molecules.clear();
    }

}
