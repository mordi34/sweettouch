package com.mordiwes.SweetTouch.ui;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Window;
import com.mordiwes.SweetTouch.R;
import com.mordiwes.SweetTouch.visualisation3d.Canvas3D;

/**
 * Created by Mordechai on 2014-09-22.
 */
public class AcknowledgmentsActivity extends Activity {
    private FragmentManager fragmentManager;
    private AcknowledgmentsFragment ackFrag;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.acknowledgments_activity);

        //setTitle("Acknowledgments");
        fragmentManager = getFragmentManager();
        setUpFragments();
    }

    private void setUpFragments() {
        ackFrag = new AcknowledgmentsFragment();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.acknowledgments_fragment_container, ackFrag);

        fragmentTransaction.commit();
    }
}