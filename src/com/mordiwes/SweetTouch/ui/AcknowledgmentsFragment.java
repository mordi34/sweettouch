package com.mordiwes.SweetTouch.ui;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.mordiwes.SweetTouch.R;

import java.util.ArrayList;

/**
 * Created by Mordechai on 2014-09-22.
 */
public class AcknowledgmentsFragment extends Fragment {
    public static final String FRAGMENT_TAG = "AcknowledgmentsFragment";

    private GridView mGridView;
    private TeamAdapter mTeamAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mGridView = (GridView) inflater.inflate(R.layout.base_gridview, null,
                false);
        mTeamAdapter = new TeamAdapter(getActivity().getApplicationContext(),DrawerPopulator.ACKNOWLEDGMENT_ITEMS);

        mGridView.setAdapter(mTeamAdapter);
        mGridView.setOnItemClickListener(mTeamAdapter);

        return mGridView;
    }

    private static final class TeamAdapter extends BaseAdapter implements
            AdapterView.OnItemClickListener {

        private final ArrayList<AcknowledgmentItem> acknowledgmentItems;
        private final Context mContext;
        private final LayoutInflater mInflater;

        public TeamAdapter(Context context, ArrayList<AcknowledgmentItem> acknowledgmentItems) {
            mContext = context;
            this.acknowledgmentItems = acknowledgmentItems;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return acknowledgmentItems.size();
        }

        @Override
        public AcknowledgmentItem getItem(int position) {
            return acknowledgmentItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup container) {
            final AcknowledgmentItem acknowledgmentItem = getItem(position);
            final ViewHolder holder;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.acknowledgment_list_item, null, false);
                holder = new ViewHolder(convertView);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.imageViewPhoto.setImageResource(acknowledgmentItem.imageResourceId);
            holder.textViewInformation.setText(acknowledgmentItem.information);
            holder.textViewName.setText(acknowledgmentItem.name);

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public void onItemClick(AdapterView<?> adapter, View view,int position, long itemID) {
            final AcknowledgmentItem acknowledgmentItem = getItem(position);

            if(acknowledgmentItem.link != null && !acknowledgmentItem.link.isEmpty()) {
                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(acknowledgmentItem.link));

                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                mContext.startActivity(intent);
            }
        }

    }

    private static final class ViewHolder {
        public final ImageView imageViewPhoto;
        public final TextView textViewName;
        public final TextView textViewInformation;

        public ViewHolder(View convertView) {
            imageViewPhoto = (ImageView) convertView.findViewById(R.id.imageViewPhoto);
            textViewInformation = (TextView) convertView.findViewById(R.id.textViewInformation);
            textViewName = (TextView) convertView.findViewById(R.id.textViewName);
            convertView.setTag(this);
        }
    }
}