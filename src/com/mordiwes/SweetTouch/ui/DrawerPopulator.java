package com.mordiwes.SweetTouch.ui;

import com.mordiwes.SweetTouch.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Adapted from Rajawali ExamplesApplication class
 * Created by Mordechai on 2014-09-21.
 */
public class DrawerPopulator {

    //items will appear in this order
    public static enum Category {

        FILE("File"),
        MODEL("Model"),
        VIEW("View"),

        ABOUT("About");

        private String name;

        Category(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

    }

    public static final Map<Category, ToolListItem[]> LIST_ITEMS = new HashMap<Category, ToolListItem[]>();
    public static final ArrayList<AcknowledgmentItem> ACKNOWLEDGMENT_ITEMS = new ArrayList<AcknowledgmentItem>();

    static  {

        LIST_ITEMS.put(Category.FILE, new ToolListItem[]{
                new ToolListItem("Load PDB File", MainActivity.Tools.LOAD_PDB_FILE),
                new ToolListItem("Search Online", MainActivity.Tools.SEARCH_ONLINE)

        });

        LIST_ITEMS.put(Category.MODEL, new ToolListItem[]{

                new ToolListItem("Ball and Stick", MainActivity.Tools.VIEW_AS_BALL_AND_STICK)
                , new ToolListItem("CPK", MainActivity.Tools.VIEW_AS_CPK)
                , new ToolListItem("Balls", MainActivity.Tools.VIEW_AS_BALLS)
                , new ToolListItem("Sticks", MainActivity.Tools.VIEW_AS_STICKS)
                , new ToolListItem("Paper Chain", MainActivity.Tools.VIEW_AS_PAPER_CHAIN)

        });

        LIST_ITEMS.put(Category.VIEW, new ToolListItem[]{
                new ToolListItem("Toggle 3D Fullscreen", MainActivity.Tools.TOGGLE_3D_FULLSCREEN),
                new ToolListItem("Toggle 2D Fullscreen", MainActivity.Tools.TOGGLE_2D_FULLSCREEN),
                new ToolListItem("Restore split view", MainActivity.Tools.RESTORE_SPLIT_VIEW),
                new ToolListItem("Toggle 3D Background Colour", MainActivity.Tools.TOGGLE_3D_BACKGROUND_COLOUR),
                new ToolListItem("Toggle 2D Background Colour", MainActivity.Tools.TOGGLE_2D_BACKGROUND_COLOUR),
                new ToolListItem("Element Colour Legend", MainActivity.Tools.SHOW_LEGEND),
                new ToolListItem("PaperChain Colour Legend", MainActivity.Tools.SHOW_PUCKERING_LEGEND)
        });

        LIST_ITEMS.put(Category.ABOUT, new ToolListItem[]{
                new ToolListItem("Acknowledgements", MainActivity.Tools.SHOW_ACKNOWLEDGMENTS),
                new ToolListItem("Help", MainActivity.Tools.SHOW_HELP)
        });


        ACKNOWLEDGMENT_ITEMS.add(new AcknowledgmentItem(
                R.drawable.michelle
                , "Michelle Kuttel"
                , "Supervisor"
                , "http://people.cs.uct.ac.za/~mkuttel/"));

        ACKNOWLEDGMENT_ITEMS.add(new AcknowledgmentItem(
                R.drawable.wes_hot_wow_plox_no_more
                , "Wesley Robinson"
                , "Developer"
                , "https://www.youtube.com/watch?v=dQw4w9WgXcQ"));

        ACKNOWLEDGMENT_ITEMS.add(new AcknowledgmentItem(
                R.drawable.mord
                , "Mordechai Serraf"
                , "Developer"
                , null));

        ACKNOWLEDGMENT_ITEMS.add(new AcknowledgmentItem(
                R.drawable.photo_rajawali3dcommunity
                ,"Rajawali 3D Library"
                , "3D visualisations powered by Rajawali OpenGL ES 2.0 Engine"
                , "https://github.com/MasDennis/Rajawali"));

    }


}
