package com.mordiwes.SweetTouch.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.Toast;
//import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
//import com.github.amlcurran.showcaseview.ShowcaseView;
//import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.mordiwes.SweetTouch.R;
import com.mordiwes.SweetTouch.elements.Structure;
import com.mordiwes.SweetTouch.filebrowser.FileChooserActivity;
import com.mordiwes.SweetTouch.identification.Canvas2D;
import com.mordiwes.SweetTouch.identification.Renderer2D;
import com.mordiwes.SweetTouch.parsing.PdbParser;
import com.mordiwes.SweetTouch.util.Constants;
import com.mordiwes.SweetTouch.visualisation3d.Canvas3D;
import com.mordiwes.SweetTouch.visualisation3d.Renderer3D;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

//http://custom-android-dn.blogspot.com/2013/01/create-simple-file-explore-in-android.html
//http://android-er.blogspot.com/2010/01/implement-simple-file-explorer-in.html

//TODO: Check if this should extend FragmentActivity instead? This seems to not think so:
//TODO: http://stackoverflow.com/questions/10477997/difference-between-activity-and-fragmentactivity
public class MainActivity extends Activity implements ExpandableListView.OnChildClickListener {//, OnShowcaseEventListener {
    public static final String TAG = "MainActivity";

    public final String URL_ENCODER_CHAR_ENCODING = "UTF-8";

    private static MainActivity instance;
    public static boolean fileChosen = false;

    { instance = this; }

    public static Context getContext() {
        return instance;
    }

    public static enum Tools {
        LOAD_PDB_FILE,
        SEARCH_ONLINE,

        VIEW_AS_BALL_AND_STICK,
        VIEW_AS_CPK,
        VIEW_AS_BALLS,
        VIEW_AS_STICKS,
        VIEW_AS_PAPER_CHAIN,

        SHOW_LEGEND,
        SHOW_PUCKERING_LEGEND,
        TOGGLE_3D_FULLSCREEN,
        TOGGLE_2D_FULLSCREEN,
        RESTORE_SPLIT_VIEW,
        TOGGLE_3D_BACKGROUND_COLOUR,
        TOGGLE_2D_BACKGROUND_COLOUR,

        SHOW_ACKNOWLEDGMENTS,
        SHOW_HELP
    }

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ExpandableListView mDrawerList;

    private CharSequence mTitle;

    public static Structure structure;
    public static float DENSITY;

    private FragmentManager fragmentManager;
    private static Canvas3D canvas3D;
    private static Canvas2D canvas2D;
    private static PdbParser parser;

    private boolean isVisualisationFragmentFullScreen = false;
    private boolean isIdentificationFragmentFullScreen = false;

//    ShowcaseView sv;

    public MainActivity() {
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        getActionBar().setIcon(R.drawable.title);
        setTitle("");

        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        DENSITY = displayMetrics.density;

        // Configure the action bar
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        parser = new PdbParser(this.getApplication());

        fragmentManager = getFragmentManager();
        setUpFragments();

        mTitle = getTitle();

        setupDrawer();

//        ViewTarget target = new ViewTarget(R.id.left_drawer, this);
//        sv = new ShowcaseView.Builder(this, true)
//                .setTarget(target)
//                .setContentTitle("SweetTouch Tutorial")
//                .setContentText("Swipe in from the left to show the options drawer")
//                .setStyle(R.style.ShowcaseButton)
//                .setShowcaseEventListener(this)
//                .build();
    }

//    @Override
//    public void onShowcaseViewHide(ShowcaseView showcaseView) {
//
//    }
//
//    @Override
//    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
//
//    }
//
//    @Override
//    public void onShowcaseViewShow(ShowcaseView showcaseView) {
//
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void setUpFragments() {
        canvas3D = new Canvas3D(new Renderer3D.Renderer3DFactory(), R.layout.fragment_visualiser);
        canvas2D = new Canvas2D(new Renderer2D.Renderer2DFactory(), R.layout.fragment_identifier);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.identifier_fragment_container, canvas2D, Canvas2D.TAG);
        fragmentTransaction.add(R.id.visualiser_fragment_container, canvas3D, Canvas3D.FRAGMENT_TAG);

        fragmentTransaction.commit();
    }

    private void setupDrawer() {
        // Configure the drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ExpandableListView) findViewById(R.id.left_drawer);
        mDrawerList.setGroupIndicator(null);
        mDrawerList.setAdapter(new ToolsListAdapter(getApplicationContext(),
                DrawerPopulator.LIST_ITEMS));
        mDrawerList.setOnChildClickListener(this);
        mDrawerList.setDrawSelectorOnTop(true);

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                       /* host Activity */
                mDrawerLayout,              /* DrawerLayout object */
                R.drawable.ic_drawer_left,  /* nav drawer imageResourceId to replace 'Up' caret */
                R.string.drawer_open,       /* "open drawer" description for accessibility */
                R.string.drawer_close       /* "close drawer" description for accessibility */
        );

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.setFocusable(false);
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        final DrawerPopulator.Category category = DrawerPopulator.Category.values()[groupPosition];
        final ToolListItem toolListItem = DrawerPopulator.LIST_ITEMS.get(category)[childPosition];
        handleLeftDrawerSelectedListItem(toolListItem);

        return true;
    }

    //TODO: Probably move a whole lot of onCreate into onStart for better convention/stability
    @Override
    public void onStart() {
        super.onStart();
    }

    // For implementation later if we need it...
    @Override
    public void onResume() {
        super.onResume();
    }

    // For implementation later if we need it...
    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void handleLeftDrawerSelectedListItem(ToolListItem toolListItem) {
        Log.d(TAG, "Execute tool: " + toolListItem.tool);

        switch (toolListItem.tool) {
            case LOAD_PDB_FILE: {
                getFile();
                break;
            }

            case SEARCH_ONLINE: {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Enter name of compound");

                // Set up the input
                final EditText input = new EditText(this);
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT);//| InputType.TYPE_TEXT_VARIATION_PASSWORD
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String name = input.getText().toString().trim();
                        String url = getPdbUrlFromStructureName(name);
                        loadStructureFromUrl(url, name);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
                break;
            }

            case SHOW_LEGEND: {
                Intent intent = new Intent(MainActivity.this, LegendActivity.class);
                startActivity(intent);
                break;
            }

            case SHOW_PUCKERING_LEGEND: {
                Intent intent = new Intent(MainActivity.this, PuckeringLegendActivity.class);
                startActivity(intent);
                break;
            }

            case VIEW_AS_BALL_AND_STICK: {
                try {
                    canvas3D.displayBallAndStickModel();
                } catch (NullPointerException ex) {
                }
                break;
                //Intent intent = new Intent(MainActivity.this, Canvas3D.class);
                // startActivity(intent);
            }

            case VIEW_AS_CPK: {
                try {
                    canvas3D.displayCpkModel();
                } catch (NullPointerException ex) {
                }
                break;
                //Intent intent = new Intent(MainActivity.this, Canvas3D.class);
                // startActivity(intent);
            }

            case VIEW_AS_BALLS: {
                try {
                    canvas3D.displayOnlyBallsAsPoints();
                } catch (NullPointerException ex) {
                }
                break;
                //Intent intent = new Intent(MainActivity.this, Canvas3D.class);
                // startActivity(intent);
            }

            case VIEW_AS_STICKS: {
                try {
                    canvas3D.displayOnlySticks();
                } catch (NullPointerException ex) {
                }
                break;
                //Intent intent = new Intent(MainActivity.this, Canvas3D.class);
                // startActivity(intent);
            }

            case VIEW_AS_PAPER_CHAIN: {
                try {
                    canvas3D.displayPaperChain();
                } catch (NullPointerException ex) {
                }
                break;
                //Intent intent = new Intent(MainActivity.this, Canvas3D.class);
                // startActivity(intent);
            }


            case TOGGLE_3D_FULLSCREEN: {
                if (!isVisualisationFragmentFullScreen) {
                    setVisualisationFragmentFullScreen();
                } else {
                    restoreSplitView();
                }
                break;
            }
            case TOGGLE_2D_FULLSCREEN: {
                if (!isIdentificationFragmentFullScreen) {
                    setIdentificationFragmentFullScreen();
                } else {
                    restoreSplitView();
                }
                break;
            }


            case RESTORE_SPLIT_VIEW: {
                restoreSplitView();

                break;
            }

            case TOGGLE_3D_BACKGROUND_COLOUR: {
                canvas3D.toggleBackgroundColour();
                break;
            }

            case TOGGLE_2D_BACKGROUND_COLOUR: {
                canvas2D.toggleBackgroundColour();
                break;
            }

            case SHOW_ACKNOWLEDGMENTS: {
                Intent intent = new Intent(MainActivity.this, AcknowledgmentsActivity.class);
                startActivity(intent);
                break;
            }

            case SHOW_HELP: {
                Intent intent = new Intent(MainActivity.this, HelpActivity.class);
                startActivity(intent);
                break;
            }

        }
        mDrawerLayout.closeDrawer(mDrawerList);
    }


    private void setVisualisationFragmentFullScreen() {
        FrameLayout visualiserLayout = (FrameLayout) findViewById(R.id.visualiser_fragment_container);
        visualiserLayout.setVisibility(View.VISIBLE);

        FrameLayout identifierLayout = (FrameLayout) findViewById(R.id.identifier_fragment_container);
        identifierLayout.setVisibility(View.GONE);

        isVisualisationFragmentFullScreen = true;
        isIdentificationFragmentFullScreen = false;
    }

    private void setIdentificationFragmentFullScreen() {
        FrameLayout visualiserLayout = (FrameLayout) findViewById(R.id.visualiser_fragment_container);
        visualiserLayout.setVisibility(View.GONE);

        FrameLayout identifierLayout = (FrameLayout) findViewById(R.id.identifier_fragment_container);
        identifierLayout.setVisibility(View.VISIBLE);

        isIdentificationFragmentFullScreen = true;
        isVisualisationFragmentFullScreen = false;
    }

    private void restoreSplitView() {
        FrameLayout visualiserLayout = (FrameLayout) findViewById(R.id.visualiser_fragment_container);
        visualiserLayout.setVisibility(View.VISIBLE);

        FrameLayout identifierLayout = (FrameLayout) findViewById(R.id.identifier_fragment_container);
        identifierLayout.setVisibility(View.VISIBLE);

        isIdentificationFragmentFullScreen = false;
        isVisualisationFragmentFullScreen = false;

    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.FILE_REQUEST_PATH:
                if (resultCode == RESULT_OK) {
                    loadStructureFromFile(data.getStringExtra("FullPath"));
                }
                break;
        }
    }

    private void loadStructureFromFile(final String file) {
        try {

            parser.parse(file);
            //this.setTitle(parser.getFileName());
            resetCanvas();

        } catch (Exception e) {
            Log.e(TAG, "Out of memory!!!!");
            Toast.makeText(getContext(), "Unable to parse PDB file", Toast.LENGTH_LONG).show();
        }
    }

    private void loadStructureFromUrl(String url, String structureName) {

        try {//"http://cactus.nci.nih.gov/chemical/structure/maltose/file?format=pdb&get3d=true"
            parser.parseUrl(url, structureName);
            //this.setTitle(parser.getFileName());
            //   resetCanvas();


        } catch (Exception e) {
            Toast.makeText(getContext(), "Unable to parse PDB file", Toast.LENGTH_LONG).show();
        }

    }

    public static void resetCanvas() {

        if (MainActivity.structure != null) {
            MainActivity.structure.clear();
        }

        MainActivity.structure = parser.getStructure();

        canvas3D.reset();
        canvas2D.reset();


        canvas3D.displayBallAndStickModel();
        canvas2D.display();
    }

    private String getPdbUrlFromStructureName(String name) {
        try {
            return Constants.PATH_TO_ONLINE_PDB_PREFIX + URLEncoder.encode(name, URL_ENCODER_CHAR_ENCODING).replaceAll("\\+", "%20") + Constants.PATH_TO_ONLINE_PDB_SUFFIX;
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    private void getFile() {
        Log.d("FileBrowsing...", "Intent sent");
        Intent intent = new Intent(this, FileChooserActivity.class);
        startActivityForResult(intent, Constants.FILE_REQUEST_PATH);
    }

}