package com.mordiwes.SweetTouch.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ListView;
import com.mordiwes.SweetTouch.R;
import com.mordiwes.SweetTouch.util.Element;
import com.mordiwes.SweetTouch.util.PeriodicTable;

import java.util.Collection;

/**
 * Created by Mordechai on 2014-11-13.
 */
public class LegendActivity extends Activity {

    private ListView mListView;
    private LegendListAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.legend_activity);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        Collection<Element> elementsCol = PeriodicTable.getElementsAsCollection();
        Element [] elements = elementsCol.toArray(new Element[elementsCol.size()]);

        mAdapter = new LegendListAdapter(this,elements);
        mListView =  (ListView) findViewById(R.id.elementsList);

        mListView.setAdapter(mAdapter);

        setTitle("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}