package com.mordiwes.SweetTouch.ui;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.mordiwes.SweetTouch.R;
import com.mordiwes.SweetTouch.elements.Ring;
import com.mordiwes.SweetTouch.util.Constants;
import com.mordiwes.SweetTouch.util.Element;

/**
 * Created by Mordechai on 2014-11-13.
 */
public class PuckeringLegendListAdapter extends ArrayAdapter<Constants.RingColouring.RingType> {
    private final int SQUARE_HIGHT = 15;
    private final int SQUARE_WIDTH = 15;
    private Element[] elements;
    private Context context;
    private final LayoutInflater mInflater;

    public PuckeringLegendListAdapter(Context context, Constants.RingColouring.RingType[] types) {
        super(context, R.layout.puckering_legend_list_item, types);

        this.context = context;
        this.elements = elements;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    /**
     * The appropriate colored square for the given element.
     *
     * @param colour
     * @return
     */
    private ColorDrawable getSquare(int colour) {

        ColorDrawable d = null;

        d = new ColorDrawable(colour);

        d.setBounds(0, 0, SQUARE_HIGHT, SQUARE_WIDTH);

        return d;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup container) {
        final Constants.RingColouring.RingType type = getItem(position);
        final ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.puckering_legend_list_item, null, false);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.imageViewColour.setImageDrawable(getSquare(type.getColour()));
        holder.textViewName.setText(type.getDescription());

        return convertView;
    }

    private static final class ViewHolder {
        public final ImageView imageViewColour;
        public final TextView textViewName;


        public ViewHolder(View convertView) {
            imageViewColour = (ImageView) convertView.findViewById(R.id.elementColourIcon);
            textViewName = (TextView) convertView.findViewById(R.id.elementName);
            convertView.setTag(this);
        }
    }
}