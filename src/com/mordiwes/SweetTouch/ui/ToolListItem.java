package com.mordiwes.SweetTouch.ui;

/**
 * Created by Mordechai on 2014-09-21.
 */
public class ToolListItem {

    public final MainActivity.Tools tool;
    public final String title;

    public ToolListItem(String title, MainActivity.Tools tool) {
        this.title = title;
        this.tool = tool;

    }

}


