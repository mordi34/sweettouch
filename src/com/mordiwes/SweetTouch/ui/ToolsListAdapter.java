package com.mordiwes.SweetTouch.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.mordiwes.SweetTouch.R;

import java.util.Map;

/**
 * Adapted from RajawaliExamples
 * Created by Mordechai on 2014-09-21.
 */
public class ToolsListAdapter extends BaseExpandableListAdapter {

        private static final int COLORS[] = new int[] { 0xFF0099CC, //0xFF9933CC,
                0xFF669900, 0xFFFF8800, 0xFFCC0000 };

        private final Map<DrawerPopulator.Category, ToolListItem[]> mItems;
        private final LayoutInflater mInflater;
        private final DrawerPopulator.Category[] mKeys;

        public ToolsListAdapter(Context context, Map<DrawerPopulator.Category, ToolListItem[]> items) {
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mItems = items;
            mKeys = DrawerPopulator.Category.values();
        }

        @Override
        public ToolListItem getChild(int groupPosition, int childPosition) {
            return mItems.get(mKeys[groupPosition])[childPosition];
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return 0;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            final ToolListItem item = getChild(groupPosition, childPosition);
            final ViewHolder holder;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.drawer_list_child_item, null);
                holder = new ViewHolder(convertView);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.textViewItemTitle.setText(item.title);


            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return mItems.get(mKeys[groupPosition]).length;
        }

        @Override
        public DrawerPopulator.Category getGroup(int groupPosition) {
            return mKeys[groupPosition];
        }

        @Override
        public int getGroupCount() {
            return mKeys.length;
        }

        @Override
        public long getGroupId(int groupPosition) {
            return 0;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            final String groupName = getGroup(groupPosition).getName();
            final ViewHolder holder;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.drawer_list_group_item, null);
                holder = new ViewHolder(convertView);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.imageViewItemColor.setBackgroundColor(COLORS[groupPosition % COLORS.length]);
            holder.textViewItemTitle.setText(groupName);

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

    private static final class ViewHolder {
        public ImageView imageViewItemColor;
        public TextView textViewItemTitle;

        public ViewHolder(View convertView) {
            imageViewItemColor = (ImageView) convertView.findViewById(R.id.item_color);
            textViewItemTitle = (TextView) convertView.findViewById(R.id.item_text);
            convertView.setTag(this);
        }
    }

    }