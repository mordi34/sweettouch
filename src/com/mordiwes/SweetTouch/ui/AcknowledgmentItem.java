package com.mordiwes.SweetTouch.ui;

/**
 * Created by Mordechai on 2014-09-21.
 */
public class AcknowledgmentItem {

        public final int imageResourceId;
        public final String name;
        public final String information;
        public final String link;

        public AcknowledgmentItem(int imageResourceId, String name, String information, String link) {
            this.imageResourceId = imageResourceId;
            this.name = name;
            this.information = information;
            this.link = link;
        }
    }
