package com.mordiwes.SweetTouch.util;

import android.graphics.Color;
import com.mordiwes.SweetTouch.elements.Atom;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is threadsafe ONLY for .get calls on the HashMap. If we were to change the map it would not be thread safe, though
 * this should never happen.
 * Created by Mordechai on 2014-09-12.
 */
public class PeriodicTable {
    private static final Map<String, Element> periodicTable = new HashMap<String, Element>();

    /*
     * Values taken from VMD PeriodicTable.C with this comment:
     *
     * van der Waals radii are taken from A. Bondi,
     * J. Phys. Chem., 68, 441 - 452, 1964,
     * except the value for H, which is taken from R.S. Rowland & R. Taylor,
     * J.Phys.Chem., 100, 7384 - 7391, 1996. Radii that are not available in
     * either of these publications have RvdW = 2.00 Å.
     * The radii for Ions (Na, K, Cl, Ca, Mg, and Cs are based on the CHARMM27
     * Rmin/2 parameters for (SOD, POT, CLA, CAL, MG, CES) by default.
     */

    public static final Element X = new Element("X", 1.50f, 0.00000);
    public static final Element H = new Element("H",0.320f, 1.20f, 1.00794, Color.WHITE);
    public static final Element He = new Element("He", 1.40f, 4.00260, Color.CYAN);
    public static final Element Li = new Element("Li", 1.82f, 6.941, Element.COLOUR_VIOLET);
    public static final Element B = new Element("B", 2.00f, 9.012182, Element.COLOUR_SALMON);
    public static final Element Be = new Element("Be", 2.00f, 10.811, Color.LTGRAY);
    public static final Element C = new Element("C",0.720f, 1.70f, 12.0107, Color.DKGRAY);
    public static final Element N = new Element("N",0.680f, 1.55f, 14.0067, Color.BLUE);
    public static final Element O = new Element("O",0.680f, 1.52f, 15.9994, Color.RED);
    public static final Element F = new Element("F", 1.47f, 18.9984032, Color.GREEN);
    public static final Element Ne = new Element("Ne", 1.54f, 20.1797, Color.CYAN);

    public static final Element Na = new Element("Na", 1.36f, 22.989770, Element.COLOUR_VIOLET);
    public static final Element Mg = new Element("Mg", 1.18f, 24.3050, Color.LTGRAY);
    public static final Element Al = new Element("Al", 2.00f, 26.981538);
    public static final Element Si = new Element("Si", 2.10f, 28.0855);
    public static final Element P = new Element("P",1.036f, 1.80f, 30.973761, Element.COLOUR_ORANGE);
    public static final Element S = new Element("S",1.020f, 1.80f, 32.065, Color.YELLOW);
    public static final Element Cl = new Element("Cl", 2.27f, 35.453, Color.GREEN);
    public static final Element Ar = new Element("Ar", 1.88f, 39.948, Color.CYAN);
    public static final Element K = new Element("K", 1.76f, 39.0983, Element.COLOUR_VIOLET);
    public static final Element Ca = new Element("Ca",0.992f, 1.37f, 40.078, Color.LTGRAY);
    public static final Element Sc = new Element("Sc", 2.00f, 44.955910);

    public static final Element Ti = new Element("Ti", 2.00f, 47.867, Color.LTGRAY);
    public static final Element V = new Element("V", 2.00f, 50.9415);
    public static final Element Cr = new Element("Cr", 2.00f, 51.9961);
    public static final Element Mn = new Element("Mn", 2.00f, 54.938049);
    public static final Element Fe = new Element("Fe",1.420f, 2.00f, 55.845, Element.COLOUR_BROWN);
    public static final Element Co = new Element("Co", 2.00f, 58.9332);
    public static final Element Ni = new Element("Ni", 1.63f, 58.6934);
    public static final Element Cu = new Element("Cu", 1.40f, 63.546);
    public static final Element Zn = new Element("Zn", 1.39f, 65.409);
    public static final Element Ga = new Element("Ga", 1.07f, 69.723);
    public static final Element Ge = new Element("Ge", 2.00f, 72.64);

    public static final Element As = new Element("As", 1.85f, 74.92160);
    public static final Element Se = new Element("Se", 1.90f, 78.96);
    public static final Element Br = new Element("Br", 1.85f, 79.904, Color.GREEN);
    public static final Element Kr = new Element("Kr", 2.02f, 83.798, Color.CYAN);
    public static final Element Rb = new Element("Rb", 2.00f, 85.4678);
    public static final Element Sr = new Element("Sr", 2.00f, 87.62);
    public static final Element Y = new Element("Y", 2.00f, 88.90585);
    public static final Element Zr = new Element("Zr", 2.00f, 91.224);
    public static final Element Nb = new Element("Nb", 2.00f, 92.90638);
    public static final Element Mo = new Element("Mo", 2.00f, 95.94);
    public static final Element Tc = new Element("Tc", 2.00f, 98.0);

    public static final Element Ru = new Element("Ru", 2.00f, 101.07);
    public static final Element Rh = new Element("Rh", 2.00f, 102.90550);
    public static final Element Pd = new Element("Pd", 1.63f, 106.42);
    public static final Element Ag = new Element("Ag", 1.72f, 107.8682);
    public static final Element Cd = new Element("Cd", 1.58f, 112.411);
    public static final Element In = new Element("In", 1.93f, 114.818);
    public static final Element Sn = new Element("Sn", 2.17f, 118.710);
    public static final Element Sb = new Element("Sb", 2.00f, 121.760);
    public static final Element Te = new Element("Tw", 2.06f, 127.60);
    public static final Element I = new Element("I", 1.98f, 126.90447, Element.COLOUR_VIOLET);
    public static final Element Xe = new Element("Xe", 2.16f, 131.293, Color.CYAN);

    public static final Element Cs = new Element("Cs", 2.10f, 132.90545);
    public static final Element Ba = new Element("Ba", 2.00f, 137.327);
    public static final Element La = new Element("La", 2.00f, 138.9055);
    public static final Element Ce = new Element("Ce", 2.00f, 140.116);
    public static final Element Pr = new Element("Pr", 2.00f, 140.90765);
    public static final Element Nd = new Element("Nd", 2.00f, 144.24);
    public static final Element Pm = new Element("Pm", 2.00f, 145.0);
    public static final Element Sm = new Element("Sm", 2.00f, 150.36);
    public static final Element Eu = new Element("Eu", 2.00f, 151.964);
    public static final Element Gd = new Element("Gd", 2.00f, 157.25);
    public static final Element Tb = new Element("Tb", 2.00f, 158.92534);

    public static final Element Dy = new Element("Dy", 2.00f, 162.500);
    public static final Element Ho = new Element("Ho", 2.00f, 164.93032);
    public static final Element Er = new Element("Er", 2.00f, 167.259);
    public static final Element Tm = new Element("Tm", 2.00f, 168.93421);
    public static final Element Yb = new Element("Yb", 2.00f, 173.04);
    public static final Element Lu = new Element("Lu", 2.00f, 174.967);
    public static final Element Hf = new Element("Hf", 2.00f, 178.49);
    public static final Element Ta = new Element("Ta", 2.00f, 180.9479);
    public static final Element W = new Element("W", 2.00f, 183.84);
    public static final Element Re = new Element("Re", 2.00f, 186.207);
    public static final Element Os = new Element("Os", 2.00f, 190.23);

    public static final Element Ir = new Element("Ir", 2.00f, 192.217);
    public static final Element Pt = new Element("Pt", 1.72f, 195.078);
    public static final Element Au = new Element("Au", 1.66f, 196.96655);
    public static final Element Hg = new Element("Hg", 1.55f, 200.59);
    public static final Element Tl = new Element("Tl", 1.96f, 204.3833);
    public static final Element Pb = new Element("Pb", 2.02f, 207.2);
    public static final Element Bi = new Element("Bi", 2.00f, 208.98038);
    public static final Element Po = new Element("Po", 2.00f, 209.0);
    public static final Element At = new Element("At", 2.00f, 210.0);
    public static final Element Rn = new Element("Rn", 2.00f, 222.0);
    public static final Element Fr = new Element("Fr", 2.00f, 223.0);

    public static final Element Ra = new Element("Ra", 2.00f, 226.0);
    public static final Element Ac = new Element("Ac", 2.00f, 227.0);
    public static final Element Th = new Element("Th", 2.00f, 232.0381);
    public static final Element Pa = new Element("Pa", 2.00f, 231.03588);
    public static final Element U = new Element("U", 1.86f, 238.02891);
    public static final Element Np = new Element("Np", 2.00f, 237.0);
    public static final Element Pu = new Element("Pu", 2.00f, 244.0);
    public static final Element Am = new Element("Am", 2.00f, 243.0);
    public static final Element Cm = new Element("Cm", 2.00f, 247.0);
    public static final Element Bk = new Element("Bk", 2.00f, 247.0);
    public static final Element Cf = new Element("Cf", 2.00f, 251.0);

    public static final Element Es = new Element("Es", 2.00f, 252.0);
    public static final Element Fm = new Element("Fm", 2.00f, 257.0);
    public static final Element Md = new Element("Md", 2.00f, 258.0);
    public static final Element No = new Element("No", 2.00f, 259.0);
    public static final Element Lr = new Element("Lr", 2.00f, 262.0);
    public static final Element Rf = new Element("Rf", 2.00f, 261.0);
    public static final Element Db = new Element("Db", 2.00f, 262.0);
    public static final Element Sg = new Element("Sg", 2.00f, 266.0);
    public static final Element Bh = new Element("Bh", 2.00f, 264.0);
    public static final Element Hs = new Element("Hs", 2.00f, 269.0);
    public static final Element Mt = new Element("Mt", 2.00f, 268.0);

    public static final Element Ds = new Element("Ds", 2.00f, 271.0);
    public static final Element Rg = new Element("Rg", 2.00f, 272.0);

    /**
     * Return Van De Waal radius for given element or throw IllegalArgumentException if none found.
     *
     * @param elementName the element for which the Van de Waal radius should be returned
     * @return the Van de Waal radius for the given element
     * @throws IllegalArgumentException if element not found
     */
    public static float getVanDeWaalRadius(String elementName) throws IllegalArgumentException {
        try {
            return periodicTable.get(getHashTableName(elementName)).vanDeWaalRadius;
        } catch (NullPointerException ex) {
            throw new IllegalArgumentException("There is no element stored as: " + elementName);
        }
    }

    public static float getVanDeWaalRadius(Atom atom) throws IllegalArgumentException {

        try {
            return getVanDeWaalRadius(atom.getElement());
        } catch (IllegalArgumentException e) {
            try {
                return getVanDeWaalRadius(atom.getName());
            } catch (IllegalArgumentException ex) {
                throw ex;
            }
        }
    }


    /**
     * Return Covalent Radius for given element or throw IllegalArgumentException if none found.
     *
     * @param elementName the element for which the Covalent Radius should be returned
     * @return the Covalent Radius radius for the given element
     * @throws IllegalArgumentException if element not found
     */
    public static float getCovalentRadius(String elementName) throws IllegalArgumentException {
        try {
            return periodicTable.get(getHashTableName(elementName)).covalentRadius;
        } catch (NullPointerException ex) {
            throw new IllegalArgumentException("There is no element stored as: " + elementName);
        }
    }

    public static float getCovalentRadius(Atom atom) throws IllegalArgumentException {

        try {
            return getCovalentRadius(atom.getElement());
        } catch (IllegalArgumentException e) {
            try {
                return getCovalentRadius(atom.getName());
            } catch (IllegalArgumentException ex) {
                throw ex;
            }
        }
    }

    public static double getMass(String elementName) {
        return periodicTable.get(getHashTableName(elementName)).mass;
    }

    public static double getMass(Atom atom) {
        return getMass(atom.getElement());
    }

    public static int getColour(String elementName) throws IllegalArgumentException {
        try {
            return periodicTable.get(getHashTableName(elementName)).colour;
        } catch (NullPointerException ex) {
            throw new IllegalArgumentException("There is no element stored as: " + elementName);
        }
    }

    public static int getColour(Atom atom) {
        try {
            return getColour(atom.getElement());
        } catch (IllegalArgumentException e) {
            try {
                return getColour(atom.getName());
            } catch (IllegalArgumentException ex) {
                return getDefaultColour();
            }
        }
    }

    public static int getDefaultColour() {
        return Element.COLOUR_PINK;
    }

    public static float getDefaultVdwRadius() {
        return 2f;
    }

    private static String getHashTableName(String elementName) {
        return elementName.toLowerCase();
    }

    public static Collection<Element> getElementsAsCollection()
    {
        return  periodicTable.values();
    }

    static {
        periodicTable.put(getHashTableName(X.name), X);
        periodicTable.put(getHashTableName(H.name), H);
        periodicTable.put(getHashTableName(He.name), He);
        periodicTable.put(getHashTableName(Li.name), Li);
        periodicTable.put(getHashTableName(B.name), B);
        periodicTable.put(getHashTableName(Be.name), Be);
        periodicTable.put(getHashTableName(C.name), C);
        periodicTable.put(getHashTableName(N.name), N);
        periodicTable.put(getHashTableName(O.name), O);
        periodicTable.put(getHashTableName(F.name), F);
        periodicTable.put(getHashTableName(Ne.name), Ne);

        periodicTable.put(getHashTableName(Na.name), Na);
        periodicTable.put(getHashTableName(Mg.name), Mg);
        periodicTable.put(getHashTableName(Al.name), Al);
        periodicTable.put(getHashTableName(Si.name), Si);
        periodicTable.put(getHashTableName(P.name), P);
        periodicTable.put(getHashTableName(S.name), S);
        periodicTable.put(getHashTableName(Cl.name), Cl);
        periodicTable.put(getHashTableName(Ar.name), Ar);
        periodicTable.put(getHashTableName(K.name), K);
        periodicTable.put(getHashTableName(Ca.name), Ca);
        periodicTable.put(getHashTableName(Sc.name), Sc);

        periodicTable.put(getHashTableName(Ti.name), Ti);
        periodicTable.put(getHashTableName(V.name), V);
        periodicTable.put(getHashTableName(Cr.name), Cr);
        periodicTable.put(getHashTableName(Mn.name), Mn);
        periodicTable.put(getHashTableName(Fe.name), Fe);
        periodicTable.put(getHashTableName(Co.name), Co);
        periodicTable.put(getHashTableName(Ni.name), Ni);
        periodicTable.put(getHashTableName(Cu.name), Cu);
        periodicTable.put(getHashTableName(Zn.name), Zn);
        periodicTable.put(getHashTableName(Ga.name), Ga);
        periodicTable.put(getHashTableName(Ge.name), Ge);

        periodicTable.put(getHashTableName(As.name), As);
        periodicTable.put(getHashTableName(Se.name), Se);
        periodicTable.put(getHashTableName(Br.name), Br);
        periodicTable.put(getHashTableName(Kr.name), Kr);
        periodicTable.put(getHashTableName(Rb.name), Rb);
        periodicTable.put(getHashTableName(Sr.name), Sr);
        periodicTable.put(getHashTableName(Y.name), Y);
        periodicTable.put(getHashTableName(Zr.name), Zr);
        periodicTable.put(getHashTableName(Nb.name), Nb);
        periodicTable.put(getHashTableName(Mo.name), Mo);
        periodicTable.put(getHashTableName(Tc.name), Tc);

        periodicTable.put(getHashTableName(Ru.name), Ru);
        periodicTable.put(getHashTableName(Rh.name), Rh);
        periodicTable.put(getHashTableName(Pd.name), Pd);
        periodicTable.put(getHashTableName(Ag.name), Ag);
        periodicTable.put(getHashTableName(Cd.name), Cd);
        periodicTable.put(getHashTableName(In.name), In);
        periodicTable.put(getHashTableName(Sn.name), Sn);
        periodicTable.put(getHashTableName(Sb.name), Sb);
        periodicTable.put(getHashTableName(Te.name), Te);
        periodicTable.put(getHashTableName(I.name), I);
        periodicTable.put(getHashTableName(Xe.name), Xe);

        periodicTable.put(getHashTableName(Cs.name), Cs);
        periodicTable.put(getHashTableName(Ba.name), Ba);
        periodicTable.put(getHashTableName(La.name), La);
        periodicTable.put(getHashTableName(Ce.name), Ce);
        periodicTable.put(getHashTableName(Pr.name), Pr);
        periodicTable.put(getHashTableName(Nd.name), Nd);
        periodicTable.put(getHashTableName(Pm.name), Pm);
        periodicTable.put(getHashTableName(Sm.name), Sm);
        periodicTable.put(getHashTableName(Eu.name), Eu);
        periodicTable.put(getHashTableName(Gd.name), Gd);
        periodicTable.put(getHashTableName(Tb.name), Tb);

        periodicTable.put(getHashTableName(Dy.name), Dy);
        periodicTable.put(getHashTableName(Ho.name), Ho);
        periodicTable.put(getHashTableName(Er.name), Er);
        periodicTable.put(getHashTableName(Tm.name), Tm);
        periodicTable.put(getHashTableName(Yb.name), Yb);
        periodicTable.put(getHashTableName(Lu.name), Lu);
        periodicTable.put(getHashTableName(Hf.name), Hf);
        periodicTable.put(getHashTableName(Ta.name), Ta);
        periodicTable.put(getHashTableName(W.name), W);
        periodicTable.put(getHashTableName(Re.name), Re);
        periodicTable.put(getHashTableName(Os.name), Os);

        periodicTable.put(getHashTableName(Ir.name), Ir);
        periodicTable.put(getHashTableName(Pt.name), Pt);
        periodicTable.put(getHashTableName(Au.name), Au);
        periodicTable.put(getHashTableName(Hg.name), Hg);
        periodicTable.put(getHashTableName(Tl.name), Tl);
        periodicTable.put(getHashTableName(Pb.name), Pb);
        periodicTable.put(getHashTableName(Bi.name), Bi);
        periodicTable.put(getHashTableName(Po.name), Po);
        periodicTable.put(getHashTableName(At.name), At);
        periodicTable.put(getHashTableName(Rn.name), Rn);
        periodicTable.put(getHashTableName(Fr.name), Fr);

        periodicTable.put(getHashTableName(Ra.name), Ra);
        periodicTable.put(getHashTableName(Ac.name), Ac);
        periodicTable.put(getHashTableName(Th.name), Th);
        periodicTable.put(getHashTableName(Pa.name), Pa);
        periodicTable.put(getHashTableName(U.name), U);
        periodicTable.put(getHashTableName(Np.name), Np);
        periodicTable.put(getHashTableName(Pu.name), Pu);
        periodicTable.put(getHashTableName(Am.name), Am);
        periodicTable.put(getHashTableName(Cm.name), Cm);
        periodicTable.put(getHashTableName(Bk.name), Bk);
        periodicTable.put(getHashTableName(Cf.name), Cf);
        periodicTable.put(getHashTableName(Es.name), Es);
        periodicTable.put(getHashTableName(Fm.name), Fm);

        periodicTable.put(getHashTableName(Es.name), Es);
        periodicTable.put(getHashTableName(Fm.name), Fm);
        periodicTable.put(getHashTableName(Md.name), Md);
        periodicTable.put(getHashTableName(No.name), No);
        periodicTable.put(getHashTableName(Lr.name), Lr);
        periodicTable.put(getHashTableName(Rf.name), Rf);
        periodicTable.put(getHashTableName(Db.name), Db);
        periodicTable.put(getHashTableName(Sg.name), Sg);
        periodicTable.put(getHashTableName(Bh.name), Bh);
        periodicTable.put(getHashTableName(Hs.name), Hs);
        periodicTable.put(getHashTableName(Mt.name), Mt);

        periodicTable.put(getHashTableName(Ds.name), Ds);
        periodicTable.put(getHashTableName(Rg.name), Rg);

    }


}
