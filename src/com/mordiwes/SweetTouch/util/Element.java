package com.mordiwes.SweetTouch.util;

import android.graphics.Color;

/**
 * Created by Mordechai on 2014-09-13.
 */
public class Element {
    public final String name;
    public final float vanDeWaalRadius;
    public float covalentRadius;
    public final double mass;
    public final int colour;

    public static int COLOUR_VIOLET = Color.argb(255,138,43,226);
    public static int COLOUR_PINK = Color.argb(255,255,20,147);
    public static int COLOUR_SALMON = Color.argb(255,233,150,122);
    public static int COLOUR_ORANGE = Color.argb(255,255,127,0);
    public static int COLOUR_BROWN = Color.argb(255,139,69,19);

    public Element(String name, float vanDeWaalRadius, double mass) {
        this.name = name;
        this.vanDeWaalRadius = vanDeWaalRadius;
        this.mass = mass;
        this.colour = COLOUR_PINK;
    }

    public Element(String name, float vanDeWaalRadius, double mass, int colour) {
        this.name = name;
        this.vanDeWaalRadius = vanDeWaalRadius;
        this.mass = mass;
        this.colour = colour;
    }

    public Element(String name,float covalentRadius, float vanDeWaalRadius, double mass, int colour) {
        this.name = name;
        this.covalentRadius = covalentRadius;
        this.vanDeWaalRadius = vanDeWaalRadius;
        this.mass = mass;
        this.colour = colour;
    }

}
