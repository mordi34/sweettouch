package com.mordiwes.SweetTouch.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import com.mordiwes.SweetTouch.R;
import com.mordiwes.SweetTouch.ui.MainActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Wesley on 9/30/2014.
 */
public class ResidueIcons {
    //Hashmap
    public static final Map<String, Bitmap> iconMap = new HashMap<String, Bitmap>();

    static {
        //UNKNOWN5
        Bitmap tempIcon = BitmapFactory.decodeResource(MainActivity.getContext().getResources(), R.drawable.unknown5);
        if (Constants.RESIDUE_ICON_SIZE != tempIcon.getWidth() || Constants.RESIDUE_ICON_SIZE != tempIcon.getHeight()) {
            Bitmap resizedIcon = getResizedBitmap(tempIcon, Constants.RESIDUE_ICON_SIZE, Constants.RESIDUE_ICON_SIZE);
            iconMap.put(ResidueTable.UNKNOWN5, resizedIcon);
        } else {
            iconMap.put(ResidueTable.UNKNOWN5, tempIcon);
        }

        //UNKNOWN6
        tempIcon = BitmapFactory.decodeResource(MainActivity.getContext().getResources(), R.drawable.unknown6);
        if (Constants.RESIDUE_ICON_SIZE != tempIcon.getWidth() || Constants.RESIDUE_ICON_SIZE != tempIcon.getHeight()) {
            Bitmap resizedIcon = getResizedBitmap(tempIcon, Constants.RESIDUE_ICON_SIZE, Constants.RESIDUE_ICON_SIZE);
            iconMap.put(ResidueTable.UNKNOWN6, resizedIcon);
        } else {
            iconMap.put(ResidueTable.UNKNOWN6, tempIcon);
        }

        //GALACTOSE
        tempIcon = BitmapFactory.decodeResource(MainActivity.getContext().getResources(), R.drawable.galactose);
        if (Constants.RESIDUE_ICON_SIZE != tempIcon.getWidth() || Constants.RESIDUE_ICON_SIZE != tempIcon.getHeight()) {
            Bitmap resizedIcon = getResizedBitmap(tempIcon, Constants.RESIDUE_ICON_SIZE, Constants.RESIDUE_ICON_SIZE);
            iconMap.put(ResidueTable.GALACTOSE, resizedIcon);
        } else {
            iconMap.put(ResidueTable.GALACTOSE, tempIcon);
        }

        //GLUCOSE
        tempIcon = BitmapFactory.decodeResource(MainActivity.getContext().getResources(), R.drawable.glucose);
        if (Constants.RESIDUE_ICON_SIZE != tempIcon.getWidth() || Constants.RESIDUE_ICON_SIZE != tempIcon.getHeight()) {
            Bitmap resizedIcon = getResizedBitmap(tempIcon, Constants.RESIDUE_ICON_SIZE, Constants.RESIDUE_ICON_SIZE);
            iconMap.put(ResidueTable.GLUCOSE, resizedIcon);
        } else {
            iconMap.put(ResidueTable.GLUCOSE, tempIcon);
        }

        //MANNOSE
        tempIcon = BitmapFactory.decodeResource(MainActivity.getContext().getResources(), R.drawable.mannose);
        if (Constants.RESIDUE_ICON_SIZE != tempIcon.getWidth() || Constants.RESIDUE_ICON_SIZE != tempIcon.getHeight()) {
            Bitmap resizedIcon = getResizedBitmap(tempIcon, Constants.RESIDUE_ICON_SIZE, Constants.RESIDUE_ICON_SIZE);
            iconMap.put(ResidueTable.MANNOSE, resizedIcon);
        } else {
            iconMap.put(ResidueTable.MANNOSE, tempIcon);
        }

        //RIBOSE
        tempIcon = BitmapFactory.decodeResource(MainActivity.getContext().getResources(), R.drawable.ribose);
        if (Constants.RESIDUE_ICON_SIZE != tempIcon.getWidth() || Constants.RESIDUE_ICON_SIZE != tempIcon.getHeight()) {
            Bitmap resizedIcon = getResizedBitmap(tempIcon, Constants.RESIDUE_ICON_SIZE, Constants.RESIDUE_ICON_SIZE);
            iconMap.put(ResidueTable.RIBOSE, resizedIcon);
        } else {
            iconMap.put(ResidueTable.RIBOSE, tempIcon);
        }

        //XYLOSE
        tempIcon = BitmapFactory.decodeResource(MainActivity.getContext().getResources(), R.drawable.xylose);
        if (Constants.RESIDUE_ICON_SIZE != tempIcon.getWidth() || Constants.RESIDUE_ICON_SIZE != tempIcon.getHeight()) {
            Bitmap resizedIcon = getResizedBitmap(tempIcon, Constants.RESIDUE_ICON_SIZE, Constants.RESIDUE_ICON_SIZE);
            iconMap.put(ResidueTable.XYLOSE, resizedIcon);
        } else {
            iconMap.put(ResidueTable.XYLOSE, tempIcon);
        }

        //FUCOSE
        tempIcon = BitmapFactory.decodeResource(MainActivity.getContext().getResources(), R.drawable.fucose);
        if (Constants.RESIDUE_ICON_SIZE != tempIcon.getWidth() || Constants.RESIDUE_ICON_SIZE != tempIcon.getHeight()) {
            Bitmap resizedIcon = getResizedBitmap(tempIcon, Constants.RESIDUE_ICON_SIZE, Constants.RESIDUE_ICON_SIZE);
            iconMap.put(ResidueTable.FUCOSE, resizedIcon);
        } else {
            iconMap.put(ResidueTable.FUCOSE, tempIcon);
        }

        //RHAMNOSE
        tempIcon = BitmapFactory.decodeResource(MainActivity.getContext().getResources(), R.drawable.rhamnose);
        if (Constants.RESIDUE_ICON_SIZE != tempIcon.getWidth() || Constants.RESIDUE_ICON_SIZE != tempIcon.getHeight()) {
            Bitmap resizedIcon = getResizedBitmap(tempIcon, Constants.RESIDUE_ICON_SIZE, Constants.RESIDUE_ICON_SIZE);
            iconMap.put(ResidueTable.RHAMNOSE, resizedIcon);
        } else {
            iconMap.put(ResidueTable.RHAMNOSE, tempIcon);
        }

        //ARABINOSE
        tempIcon = BitmapFactory.decodeResource(MainActivity.getContext().getResources(), R.drawable.arabinose);
        if (Constants.RESIDUE_ICON_SIZE != tempIcon.getWidth() || Constants.RESIDUE_ICON_SIZE != tempIcon.getHeight()) {
            Bitmap resizedIcon = getResizedBitmap(tempIcon, Constants.RESIDUE_ICON_SIZE, Constants.RESIDUE_ICON_SIZE);
            iconMap.put(ResidueTable.ARABINOSE, resizedIcon);
        } else {
            iconMap.put(ResidueTable.ARABINOSE, tempIcon);
        }

        //LYXOSE
        tempIcon = BitmapFactory.decodeResource(MainActivity.getContext().getResources(), R.drawable.lyxose);
        if (Constants.RESIDUE_ICON_SIZE != tempIcon.getWidth() || Constants.RESIDUE_ICON_SIZE != tempIcon.getHeight()) {
            Bitmap resizedIcon = getResizedBitmap(tempIcon, Constants.RESIDUE_ICON_SIZE, Constants.RESIDUE_ICON_SIZE);
            iconMap.put(ResidueTable.LYXOSE, resizedIcon);
        } else {
            iconMap.put(ResidueTable.LYXOSE, tempIcon);
        }

        //ALLOSE
        tempIcon = BitmapFactory.decodeResource(MainActivity.getContext().getResources(), R.drawable.allose);
        if (Constants.RESIDUE_ICON_SIZE != tempIcon.getWidth() || Constants.RESIDUE_ICON_SIZE != tempIcon.getHeight()) {
            Bitmap resizedIcon = getResizedBitmap(tempIcon, Constants.RESIDUE_ICON_SIZE, Constants.RESIDUE_ICON_SIZE);
            iconMap.put(ResidueTable.ALLOSE, resizedIcon);
        } else {
            iconMap.put(ResidueTable.ALLOSE, tempIcon);
        }

        //GULOSE
        tempIcon = BitmapFactory.decodeResource(MainActivity.getContext().getResources(), R.drawable.gulose);
        if (Constants.RESIDUE_ICON_SIZE != tempIcon.getWidth() || Constants.RESIDUE_ICON_SIZE != tempIcon.getHeight()) {
            Bitmap resizedIcon = getResizedBitmap(tempIcon, Constants.RESIDUE_ICON_SIZE, Constants.RESIDUE_ICON_SIZE);
            iconMap.put(ResidueTable.GULOSE, resizedIcon);
        } else {
            iconMap.put(ResidueTable.GULOSE, tempIcon);
        }

        //ALTROSE
        tempIcon = BitmapFactory.decodeResource(MainActivity.getContext().getResources(), R.drawable.altrose);
        if (Constants.RESIDUE_ICON_SIZE != tempIcon.getWidth() || Constants.RESIDUE_ICON_SIZE != tempIcon.getHeight()) {
            Bitmap resizedIcon = getResizedBitmap(tempIcon, Constants.RESIDUE_ICON_SIZE, Constants.RESIDUE_ICON_SIZE);
            iconMap.put(ResidueTable.ALTROSE, resizedIcon);
        } else {
            iconMap.put(ResidueTable.ALTROSE, tempIcon);
        }

        //dgalf
        tempIcon = BitmapFactory.decodeResource(MainActivity.getContext().getResources(), R.drawable.dgalf);
        if (Constants.RESIDUE_ICON_SIZE != tempIcon.getWidth() || Constants.RESIDUE_ICON_SIZE != tempIcon.getHeight()) {
            Bitmap resizedIcon = getResizedBitmap(tempIcon, Constants.RESIDUE_ICON_SIZE, Constants.RESIDUE_ICON_SIZE);
            iconMap.put(ResidueTable.DGalf, resizedIcon);
        } else {
            iconMap.put(ResidueTable.DGalf, tempIcon);
        }

        //dribf
        tempIcon = BitmapFactory.decodeResource(MainActivity.getContext().getResources(), R.drawable.dribf);
        if (Constants.RESIDUE_ICON_SIZE != tempIcon.getWidth() || Constants.RESIDUE_ICON_SIZE != tempIcon.getHeight()) {
            Bitmap resizedIcon = getResizedBitmap(tempIcon, Constants.RESIDUE_ICON_SIZE, Constants.RESIDUE_ICON_SIZE);
            iconMap.put(ResidueTable.DRibf, resizedIcon);
        } else {
            iconMap.put(ResidueTable.DRibf, tempIcon);
        }

        //dxylf
        tempIcon = BitmapFactory.decodeResource(MainActivity.getContext().getResources(), R.drawable.dxylf);
        if (Constants.RESIDUE_ICON_SIZE != tempIcon.getWidth() || Constants.RESIDUE_ICON_SIZE != tempIcon.getHeight()) {
            Bitmap resizedIcon = getResizedBitmap(tempIcon, Constants.RESIDUE_ICON_SIZE, Constants.RESIDUE_ICON_SIZE);
            iconMap.put(ResidueTable.DXylf, resizedIcon);
        } else {
            iconMap.put(ResidueTable.DXylf, tempIcon);
        }

        //FRUCTOSE
        tempIcon = BitmapFactory.decodeResource(MainActivity.getContext().getResources(), R.drawable.fructose);
        if (Constants.RESIDUE_ICON_SIZE != tempIcon.getWidth() || Constants.RESIDUE_ICON_SIZE != tempIcon.getHeight()) {
            Bitmap resizedIcon = getResizedBitmap(tempIcon, Constants.RESIDUE_ICON_SIZE, Constants.RESIDUE_ICON_SIZE);
            iconMap.put(ResidueTable.FRUCTOSE, resizedIcon);
        } else {
            iconMap.put(ResidueTable.FRUCTOSE, tempIcon);
        }

        //LINKAGE_BLOCK
        tempIcon = BitmapFactory.decodeResource(MainActivity.getContext().getResources(), R.drawable.linkageblock);
        iconMap.put(DrawUtils.LINKAGE_BLOCK, tempIcon);
    }

    public static Bitmap getIconForResidue(String residueName) {
        Bitmap icon = iconMap.get(residueName);
        return icon == null ? iconMap.get(ResidueTable.UNKNOWN6) : icon;
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();

        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        return Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
    }
}
