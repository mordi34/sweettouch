package com.mordiwes.SweetTouch.util;

import com.mordiwes.SweetTouch.elements.Atom;
import rajawali.math.Number3D;

import java.util.ArrayList;
import java.util.List;

/**
 * Vector3d class based on https://code.google.com/p/loon-simple/source/browse/trunk/android/LGame-Android-0.2.7/org/loon/framework/android/game/action/map/shapes/Vector3D.java?spec=svn26&r=26
 * Created by Wesley on 9/10/2014.
 */
public class Vector3d {
    public double x, y, z;

    public Vector3d()
    {

    }

    public Vector3d(Number3D number3D)
    {
        this.x = number3D.x;
        this.y = number3D.y;
        this.z = number3D.z;
    }

    public Vector3d(Vector3d vector3d) {
        this.x = vector3d.x;
        this.y = vector3d.y;
        this.z = vector3d.z;
    }

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {

        return x;
    }

    public void setAll(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;

    }

    public void setAll(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;

    }

    public void setAll(Vector3d other)
    {
        this.x = other.x;
        this.y = other.y;
        this.z = other.z;

    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public int x() {
        return (int) x;
    }

    public int y() {
        return (int) y;
    }

    public int z() {
        return (int) z;
    }

    public Object clone() {
        return new Vector3d(x, y, z);
    }

    public void move(Vector3d vector3d) {
        this.x += vector3d.x;
        this.y += vector3d.y;
        this.z += vector3d.z;
    }

    public void move(double x, double y, double z) {
        this.x += x;
        this.y += y;
        this.z += z;
    }

    public double[] getCoordsAsArray() {
        return (new double[]{x, y, z});
    }

    public boolean equals(Object rhs) {
        if (rhs instanceof Vector3d) {
            Vector3d newVec = (Vector3d) rhs;
            return this.x == newVec.x && this.y == newVec.y && this.z == newVec.z;
        }
        return false;
    }

    /**
     * ************************
     * Vector Maths operations *
     * *************************
     */

    public Vector3d add(Vector3d other) {
        return new Vector3d(
                this.x + other.x,
                this.y + other.y,
                this.z + other.z);
    }

    public Vector3d subtract(Vector3d other) {
        return new Vector3d(
                this.x - other.x,
                this.y - other.y,
                this.z - other.z);
    }

    public static Vector3d subtract (Vector3d lhs, Vector3d rhs) {
        return lhs.subtract(rhs);
    }

    public Vector3d multiply (double value) {
        return new Vector3d(
                this.x * value,
                this.y * value,
                this.z * value);
    }

    public Vector3d crossProduct(Vector3d other) {
        double x = this.y * other.z - other.y * this.z;
        double y = this.z * other.x - other.z * this.x;
        double z = this.x * other.y - other.x * this.y;
        return new Vector3d(x, y, z);
    }

    public static Vector3d crossProduct (Vector3d lhs, Vector3d rhs) {
        return lhs.crossProduct(rhs);
    }

    public double dotProduct (Vector3d other) {
        return other.x * x + other.y * y + other.z * z;
    }

    public static double dotProduct (Vector3d lhs, Vector3d rhs) {
        return lhs.dotProduct(rhs);
    }

    public Vector3d normalise () {
        double magnitude = Math.sqrt(dotProduct(this));
        return new Vector3d(x / magnitude, y / magnitude, z / magnitude);
    }

    public double level() {
        return Math.sqrt(dotProduct(this));
    }

    public Vector3d modulate(Vector3d other) {
        double x = this.x * other.x;
        double y = this.y * other.y;
        double z = this.z * other.z;
        return new Vector3d(x, y, z);
    }

    public static Vector3d findCentroid (List<Vector3d> vertices)
    {
        Vector3d centroid;
        double xTot= 0;
        double yTot= 0;
        double zTot= 0;

        for (Vector3d vector3d: vertices)
        {
            xTot += vector3d.x;
            yTot += vector3d.y;
            zTot += vector3d.z;
        }

        centroid = new Vector3d(xTot/vertices.size(), yTot/vertices.size(), zTot/vertices.size());

        return centroid;
    }

//    public static Vector3d findCentroid(List<Atom> atoms)
//    {
//        ArrayList <Vector3d> points = new ArrayList<Vector3d>();
//        for (Atom atom: atoms)
//        {
//            points.add(atom.getLocation());
//        }
//
//        return findCentroid(points);
//    }

    public static Number3D toNumber3D (Vector3d vector3d)
    {
        return new Number3D(vector3d.x,vector3d.y,vector3d.z);
    }

    public String toString() {
        return (new StringBuffer("[Vector3D (")).append(x).append(", ")
                .append(y).append(", ").append(z).append(") ]").toString();
    }
}
