package com.mordiwes.SweetTouch.util;

import android.graphics.Color;
import rajawali.math.Number3D;

/**
 * Created by Wesley on 10/5/2014.
 */
public class DrawUtils {
    public static final String TAG = "DrawUtils";

    public static final String LINKAGE_BLOCK = "LINKAGE_BLOCK";

    public static final float IMAGE_RADIUS = 0.1f;
    public static final float IMAGE_HEIGHT = 2*IMAGE_RADIUS;
    public static final float IMAGE_WIDTH = 2*IMAGE_RADIUS;

    public static final float BLOCK_SIZE = 0.05f;
    public static final float X_SHIFT = 0.15f;
    public static final float Y_SHIFT = 0.15f;

    public static final float X_BEGIN_POSITION = 0f;
    public static final float Y_BEGIN_POSITION = 0f;

    public static final float CONNECTOR_WIDTH = 7f;
    public static final float CONNECTOR_LENGTH = 0.2f;
    public static final int CONNECTOR_COLOUR = Color.DKGRAY;

    public static final float DISTANCE_BETWEEN_RESIDUES = CONNECTOR_LENGTH + IMAGE_RADIUS;

    public static final Number3D EAST = new Number3D(DISTANCE_BETWEEN_RESIDUES, 0f, 0f);

    public static final Number3D NORTH = new Number3D(0f, DISTANCE_BETWEEN_RESIDUES, 0f);

    public static final Number3D WEST = new Number3D(-DISTANCE_BETWEEN_RESIDUES, 0f, 0f);

    public static final Number3D SOUTH = new Number3D(0f, -DISTANCE_BETWEEN_RESIDUES, 0f);

    public static Number3D getVertexCoordinates(float x, float y, float radius, int numOfPt, int pointOfInterest) {
        double section = 2.0 * Math.PI/numOfPt;

        Number3D point = new Number3D((float) (x + radius * Math.cos(section * pointOfInterest)),
                (float) (y - radius * Math.sin(section * pointOfInterest)), 0f);

        if (Math.abs(point.x) < 0.001) point.x = 0f;
        if (Math.abs(point.y) < 0.001) point.y = 0f;

        return point;
    }

    @Deprecated
    public static Number3D getCenterCoordinates(Number3D point, float radius, int numOfPt, int pointOfInterest) {
        double section = 2.0 * Math.PI/numOfPt;

        Number3D center = new Number3D((float) (point.x - radius * Math.cos(section * pointOfInterest)),
                (float) (point.y + radius * Math.sin(section * pointOfInterest)), 0f);

        if (Math.abs(center.x) < 0.001) center.x = 0f;
        if (Math.abs(center.y) < 0.001) center.y = 0f;

        return center;
    }
}
