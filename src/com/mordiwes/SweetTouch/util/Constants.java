package com.mordiwes.SweetTouch.util;

import android.graphics.Color;
import rajawali.math.Number3D;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wesley on 9/30/2014.
 */
public class Constants {
    //General
    public static final String DEFAULT_DIRECTORY = "/storage/emulated/0/Sweet Touch";
    public static final String DEFAULT_PDB = "maltose.pdb";

    public static final int FILE_REQUEST_PATH = 0;


    public static final String PATH_TO_ONLINE_PDB_PREFIX = "http://cactus.nci.nih.gov/chemical/structure/";
    public static final String PATH_TO_ONLINE_PDB_SUFFIX = "/file?format=pdb&get3d=true";

    public static final float INITIAL_SCALE = 1f;
    public static final float MAX_SCALE = 2.5f;
    public static final float MIN_SCALE = 0.2f;

    //Identification
    public static final int RESIDUE_ICON_SIZE = 256;


    //Rings
    public static class RingColouring {

//        public static int PLANAR_RED = Color.argb(255, 255, 0, 0);
//        public static int CHAIR_GREEN = Color.argb(255, 0, 255, 0);
//        public static int BOAT_BLUE = Color.argb(255, 0, 0, 255);
//        public static int HALF_CHAIR_PURPLE = Color.argb(255, 154,50,205);
//        public static int SKEW_BOAT_ORANGE = Color.argb(255, 255,127,0);
//        public static int RING6_ENVELOPE_YELLOW = Color.YELLOW;
//        public static int RING5_TWIST_PINK = Color.argb(255, 255,20,147);
//        public static int RING5_ENVELOPE_CYAN = Color.CYAN;
//        public static int RING7_BROWN = Color.argb(255,139,69,19);
//        public static int DEFAULT_GRAY = Color.GRAY;
        public enum RingType {
            PLANAR(new int[]{255, 255, 0, 0},"Planar"),//red
            CHAIR(new int[]{255, 0, 255, 0},"Chair"),//green
            BOAT(new int[]{255, 0, 0, 255},"Boat"),//blue
            HALF_CHAIR(new int[]{255, 154,50,205},"Half Chair"),//purple
            SKEW_BOAT(new int[]{255, 255,127,0},"Skew Boat"),//orange
            RING6_ENVELOPE(new int[]{255,255,255,0},"Envelope (6 Ring)"),//yellow
            RING5_TWIST( new int[]{255, 255,20,147},"Twist (5 Ring)"),//pink
            RING5_ENVELOPE( new int[]{255,135,206,250},"Envelope (5 Ring)"),//cyan
            RING7( new int[]{255,139,69,19}, "7 Ring"),//brown
            DEFAULT( new int[]{255,192,192,192}, "Default");//grey


            private int colour;
            private String description;
            private int [] argb;

            RingType(int [] argb, String description)
            {
                this.description = description;
                this.colour = Color.argb(argb[0],argb[1],argb[2],argb[3]);
                this.argb = new int[argb.length];
                for (int i = 0; i< this.argb.length; i++)
                {
                    this.argb[i] = argb[i];
                }
            }

            public  int getColour()
            {
                return colour;
            }

            public String getDescription (){return description;}

            public int getLightenedColour()
            {
                return Constants.RingColouring.lightenColour(argb);
            }

            public  int[] getArgb()
            {
                return argb.clone();
            }

        }


        public static float [][] sixRings = {
            {0.0f, 0.0f, 0.0f},          //Planar
            {-35.26f, -35.26f, -35.26f},	//Chairs
            {35.26f, 35.26f, 35.26f},
            {-35.26f, 74.20f, -35.26f},	//Boats
            {35.26f, -74.20f, 35.26f},
            {74.20f, -35.26f, -35.26f},
            {-74.20f, 35.26f, 35.26f},
            {-35.26f, -35.26f, 74.20f},
            {35.26f, 35.26f, -74.20f},
            {-42.16f, 9.07f, -17.83f},   //Half-Chairs
            {42.16f, -9.07f, 17.83f},
            {42.16f, 17.83f, -9.06f},
            {-42.16f, -17.83f, 9.06f},
            {-17.83f, -42.16f, 9.07f},
            {17.83f, 42.16f, -9.07f},
            {-9.07f, 42.16f, 17.83f},
            {9.07f, -42.16f, -17.83f},
            {9.07f, -17.83f, -42.16f},
            {-9.07f, 17.83f, 42.16f},
            {17.83f, -9.07f, 42.16f},
            {-17.83f, 9.07f, -42.16f},
            {0.0f, 50.84f, -50.84f},    //Skew-Boats
            {0.0f, -50.48f, 50.48f},
            {50.84f, -50.84f, 0.0f},
            {-50.48f, 50.48f, 0.0f},
            {-50.48f, 0.0f, 50.48f},
            {50.48f, 0.0f, -50.48f},
            {-35.26f, 17.37f, -35.26f}, //Envelopes
            {35.26f, -17.37f, 35.26f},
            {46.86f, 0.0f, 0.0f},
            {-46.86f, 0.0f, 0.0f},
            {-35.26f, -35.26f, 17.37f},
            {35.26f, 35.26f, -17.37f},
            {0.0f, 46.86f, 0.0f},
            {0.0f, -46.86f, 0.0f},
            {17.37f, -35.26f, -35.26f},
            {-17.37f, 35.26f, 35.26f},
            {0.0f, 0.0f, 46.86f},
            {0.0f, 0.0f, -46.86f} };

        public static float fiveRings[][] = {
            {0.0f, 0.0f},       //Planar
            {-13.10f, -33.89f}, //Twists
            {-42.16f, 13.21f},
            {34.50f, -34.50f},
            {-13.21f, 42.16f},
            {33.89f, 13.11f},
            {13.10f, 33.89f},
            {42.16f, -13.21f},
            {-34.50f, 34.50f},
            {13.21f, -42.16f},
            {-33.89f, -13.11f},
            {-24.88f, 40.00f},  //Envelopes
            {0.00f, -39.90f},
            {24.50f, 24.50f},
            {-39.50f, 0.00f},
            {39.50f, -24.90f},
            {24.88f, -40.00f},
            {0.00f, 39.90f},
            {-24.50f, -24.50f},
            {39.50f, 0.00f},
            {-39.50f, 24.90f}};

        //Returns the position in the array of 6ring conformations to which the Hill Reilly angles correspond
        public static int sixRingPosition(List<Double> angles) {

            int pos = -1;
            float range = 15.0f;

            //Find the best matching conformation using a sum of squared differences between the angles
            float sum = 100000.0f;
            for (int i = 0; i<=38; i++){
                float temp = (float) (Math.pow(angles.get(0) - sixRings[i][0], 2) +  Math.pow(angles.get(1) - sixRings[i][1], 2) +  Math.pow(angles.get(2) - sixRings[i][2], 2));
                if(temp < sum) {
                    sum = temp;
                    pos = i;
                }
            }

            //Check if it is a tight match to the ideal conformation
            if(!((sixRings[pos][0]-range) < angles.get(0) && angles.get(0) < (sixRings[pos][0]+range) &&
                    (sixRings[pos][1]-range) < angles.get(1)&& angles.get(1) < (sixRings[pos][1]+range) &&
                    (sixRings[pos][2]-range) < angles.get(2) && angles.get(2) < (sixRings[pos][2]+range))) {
                pos *= -1;
            }

            return pos;
        }


        //Returns the position in the array of 5ring conformations to which the Hill Reilly angles correspond
        public static int fiveRingPosition(List<Double> angles)
        {
            int pos = -1;
            float range = 10.0f;

            //Find the best matching conformation using a sum of squared differences between the angles
            float sum = 100000.0f;
            for (int i = 0; i<=20; i++){//was 21?!
                float temp = (float) (Math.pow(angles.get(0) - fiveRings[i][0], 2) + Math.pow(angles.get(1) - fiveRings[i][1], 2));
                if(temp < sum) {
                    sum = temp;
                    pos = i;
                }
            }

            //Check if it is a tight match to the ideal conformation
            if((fiveRings[pos][0]-range) < angles.get(0) && angles.get(0) < (fiveRings[pos][0]+range) &&
                    (fiveRings[pos][1]-range) < angles.get(1) && angles.get(1) < (fiveRings[pos][1]+range)) {
                pos *= -1;
            }

            return pos;
        }

        public static int lightenColour (int [] argb)
        {
            argb[0] = argb[0] + (4 * (255- argb[0])) / 10;
            argb[1] = argb[1] + (4 * (255 - argb[1])) / 10;
            argb[2] = argb[2] + (4 * (255- argb[2])) / 10;

            return  Color.argb(argb[0],argb[1],argb[2], argb[3]);
        }

    }
}
