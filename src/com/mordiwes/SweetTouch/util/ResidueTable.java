package com.mordiwes.SweetTouch.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Wesley on 9/22/2014.
 */
public class ResidueTable {
    public static final Map<String, String> residueTable = new HashMap<String, String>();

    public static final String UNKNOWN6 = "UNKNOWN6";
    public static final String UNKNOWN5 = "UNKNOWN5";

    public static final String GLUCOSE = "GLUCOSE";
    public static final String GALACTOSE = "GALACTOSE";
    public static final String MANNOSE = "MANNOSE";
    public static final String RIBOSE = "RIBOSE";
    public static final String XYLOSE = "XYLOSE";
    public static final String FUCOSE = "FUCOSE";
    public static final String RHAMNOSE = "RHAMNOSE";
    public static final String ARABINOSE = "ARABINOSE";
    public static final String LYXOSE = "LYXOSE";
    public static final String ALLOSE = "ALLOSE";
    public static final String GULOSE = "GULOSE";
    public static final String ALTROSE = "ALTROSE";
    public static final String DGalf = "dgalf";
    public static final String DRibf = "dribf";
    public static final String DXylf = "dxylf";
    public static final String FRUCTOSE = "FRUCTOSE";

    public static String getResidueName(String stereocode) {
        return residueTable.get(stereocode);
    }

    static {
        residueTable.put("61212", GLUCOSE);
        residueTable.put("61211", MANNOSE);
        residueTable.put("61112", GALACTOSE);
        residueTable.put("6_222", RIBOSE);
        residueTable.put("6_121", XYLOSE);
        residueTable.put("62221", FUCOSE);
        residueTable.put("62122", RHAMNOSE);
        residueTable.put("6_112", ARABINOSE);
        residueTable.put("6_211", LYXOSE);
        residueTable.put("61222", ALLOSE);
        residueTable.put("61122", GULOSE);
        residueTable.put("61221", ALTROSE);
        residueTable.put("5212", DGalf);
        residueTable.put("5122", DRibf);
        residueTable.put("5112", DXylf);
        residueTable.put("5121", FRUCTOSE);
    }
}
