package com.mordiwes.SweetTouch.identification;

import android.content.Context;
import android.util.Log;
import com.mordiwes.SweetTouch.elements.RingLinkage;
import com.mordiwes.SweetTouch.util.*;
import com.mordiwes.SweetTouch.display.SweetTouchRendererFactory;
import com.mordiwes.SweetTouch.display.SweetTouchBaseObject3D;
import com.mordiwes.SweetTouch.display.SweetTouchRenderer;
import rajawali.BaseObject3D;
import rajawali.Camera2D;
import rajawali.bounds.BoundingBox;
import rajawali.lights.DirectionalLight;
import com.mordiwes.SweetTouch.elements.Molecule;
import com.mordiwes.SweetTouch.elements.Ring;
import com.mordiwes.SweetTouch.ui.MainActivity;
import rajawali.materials.DiffuseMaterial;
import rajawali.materials.SimpleMaterial;
import rajawali.math.Number3D;
import rajawali.primitives.Line3D;
import rajawali.primitives.Plane;

import javax.microedition.khronos.opengles.GL10;
import java.util.ArrayList;
import java.util.Stack;

/**
 * Created by Wesley on 10/2/2014.
 */
public class Renderer2D extends SweetTouchRenderer {
    public static final String TAG = "Renderer2D";

    private Camera2D camera2D;

    private DirectionalLight mLight;

    private SweetTouchBaseObject3D shapes;
    private SweetTouchBaseObject3D connectors;
    private SweetTouchBaseObject3D allShapes;

    private Molecule currentMolecule;

    DiffuseMaterial material;
    Plane icon;

    private boolean neverDisplay = false;

    private boolean newFlagToDisplay = false;

    public Renderer2D (Context context) {
        super(context, 60);

        setBackgroundColor(0.85f,0.85f,0.85f,1f);
        mBackgroundColourToggleCount++;

        shapes = new SweetTouchBaseObject3D();
        connectors = new SweetTouchBaseObject3D();
        allShapes = new SweetTouchBaseObject3D();

        material = new DiffuseMaterial();
    }

    @Override
    protected void initScene() {
        setUpLights();
        setUpCamera();
    }

    @Override
    public void onDrawFrame(GL10 glUnused) {
        super.onDrawFrame(glUnused);

        if (flagNewRequestToReset) {
            flagNewRequestToReset = false;

            destroyScene();

            mScale = Constants.INITIAL_SCALE;

            shapes = new SweetTouchBaseObject3D();
            connectors = new SweetTouchBaseObject3D();
            allShapes = new SweetTouchBaseObject3D();

            allShapes.setScale(mScale);

            material = new DiffuseMaterial();

            neverDisplay = false;
        }

        if(newFlagToDisplay) {
            newFlagToDisplay = false;
            //perhaps put check here to see if already setup
            try {
                setUpChildren();
            } catch (Exception e) {
                neverDisplay = true;
            }
        }

    }

    public void display() {
        if (!neverDisplay) {
            newFlagToDisplay = true;
        }
    }

    private void setUpChildren() throws Exception {
        for (Molecule molecule : MainActivity.structure.getMolecules()) {
            currentMolecule = molecule;

            float xPos = DrawUtils.X_BEGIN_POSITION;
            float yPos = DrawUtils.Y_BEGIN_POSITION;

            // Attempt at tail recursive implementation
            createRings(molecule.getRings().size()-1, xPos, yPos);

            ArrayList<RingLinkage> displayedRingLinks = new ArrayList<RingLinkage>();

            for (Ring ring : molecule.getRings()) {
                createLinks(ring, displayedRingLinks);
            }
        }

        connectors.setZ(-1f);   //TODO: Make this thing

        allShapes.addChild(shapes);
        allShapes.addChild(connectors);

        allShapes.addLight(mLight);

        addChild(allShapes);
    }

    /**
     * Recursively draws rings to the screen
     * @param ringID ID of the ring, so that it can be extracted from currentMolecule
     * @param xPos X coordinate of the centre of the ring
     * @param yPos Y coordinate of the centre of the ring
     */
    private void createRings(int ringID, float xPos, float yPos) throws Exception {
        Ring ring = currentMolecule.getRings().get(ringID);

        ring.visited = true;
        ring.setDisplayPos2D(xPos, yPos);
        ring.positioned = true;

//        Log.e(TAG, ring.getRingID() + ":" + ring.getResidueName() + " " + ring.getNeighbourRings().size());

        shapes.addChild(getIcon(ring, xPos, yPos));    //Draw the residue icon

        Number3D linkageBlockPos = DrawUtils.getVertexCoordinates(xPos, yPos, DrawUtils.IMAGE_RADIUS, ring.getNumberOfAtoms(), 0);
        shapes.addChild(getLinkageIcon(DrawUtils.LINKAGE_BLOCK, linkageBlockPos.x, linkageBlockPos.y));   //Draw the linkage block

        Stack<Ring> ringsToCreate = new Stack<Ring>();

        for (RingLinkage ringLinkage : ring.getRingLinkages()) {
//            Log.e(TAG, ".." + ringLinkage.getOtherRing().getRingID() + ":" + ringLinkage.getOtherRing().getResidueName() + ":" + ringLinkage.getOtherRing().positioned + ":" + ringLinkage.getLinkExitPosition() + "," + ringLinkage.getLinkEnterPosition());
            Ring neighbourRing = ringLinkage.getOtherRing();
            if (!neighbourRing.positioned) {

                neighbourRing.positioned = true;

                Number3D nextAtomDirection = getNextAtomDirection(ringLinkage.getLinkExitPosition(), ringLinkage.getLinkEnterPosition(),
                        ring.getNumberOfAtoms(), neighbourRing.getNumberOfAtoms());

                Log.e(TAG, "Test" + nextAtomDirection.x + "");

                neighbourRing.setDisplayPos2D(xPos + nextAtomDirection.x, yPos + nextAtomDirection.y);

                ringsToCreate.push(neighbourRing);
//                Log.e(TAG, ".." + neighbourRing.getRingID() + ":" + neighbourRing.getResidueName() + ":" + neighbourRing.getDisplayPos2D().x + "," + neighbourRing.getDisplayPos2D().y);
            }
        }

        while (ringsToCreate.size() > 0) {
            Ring ringToCreate = ringsToCreate.pop();
            createRings(ringToCreate.getRingID(), ringToCreate.getDisplayPos2D().x, ringToCreate.getDisplayPos2D().y);
        }

//        for (RingLinkage ringLinkage : ring.getRingLinkages()) {
//            if (!ringLinkage.getOtherRing().isVisited()) {
//                Ring neighbourRing = ringLinkage.getOtherRing();
//
//                createRings(neighbourRing.getRingID(), neighbourRing.getDisplayPos2D().x, neighbourRing.getDisplayPos2D().y);
//            }
//        }
    }

    private BaseObject3D getLinkageIcon(String linkageBlock, float xPos, float yPos) {
        material = new DiffuseMaterial();
        icon = new Plane(DrawUtils.BLOCK_SIZE, DrawUtils.BLOCK_SIZE, 5, 5);


        icon.setMaterial(material);
        icon.addTexture(mTextureManager.addTexture(ResidueIcons.getIconForResidue(DrawUtils.LINKAGE_BLOCK)));
        icon.setTransparent(true);
        icon.setX(xPos);
        icon.setY(yPos);
        icon.setZ(-0.5f);

        return icon;
    }

    private void createLinks(Ring ring, ArrayList<RingLinkage> displayedRingLinks) throws Exception{
        for (RingLinkage ringLinkage : ring.getRingLinkages()) {
            if (!displayedRingLinks.contains(ringLinkage)) {
                Ring neighbourRing = ringLinkage.getOtherRing();

                Stack<Number3D> linkagePoints = new Stack<Number3D>();

                linkagePoints.push(DrawUtils.getVertexCoordinates(
                        ring.getDisplayPos2D().x,
                        ring.getDisplayPos2D().y,
                        DrawUtils.IMAGE_RADIUS,
                        ring.getNumberOfAtoms(),
                        ringLinkage.getLinkExitPosition()-1));

                linkagePoints.push(DrawUtils.getVertexCoordinates(
                        neighbourRing.getDisplayPos2D().x,
                        neighbourRing.getDisplayPos2D().y,
                        DrawUtils.IMAGE_RADIUS,
                        neighbourRing.getNumberOfAtoms(),
                        ringLinkage.getLinkEnterPosition()-1));

                connectors.addChild(getConnector(linkagePoints));
                displayedRingLinks.add(ringLinkage);
            }
        }
    }

    private Number3D getNextAtomDirection(int exitAtom, int entryAtom, int exitRingSize, int entryRingSize) {
        Log.e(TAG, exitAtom + " " + entryAtom + " exitSize:" + exitRingSize + " entrySize" + entryRingSize);
        if (    exitRingSize  < 5 || exitRingSize  > 6 ||
                entryRingSize < 5 || entryRingSize > 6 ||
                exitAtom  < 0 || exitAtom  > exitRingSize ||
                entryAtom < 0 || entryAtom > entryRingSize) {
            return null;    //STOP RIGHT THERE INTRUDER
        }

        Number3D nextAtomDirection = new Number3D();

        if (exitRingSize == 6) {
            if (entryRingSize == 6) {
                if (entryAtom < 4 && entryAtom > 1 ||
                        exitAtom <= 6 && exitAtom > 4) {
                    nextAtomDirection.add(DrawUtils.NORTH);
                    if (exitAtom == 1 || entryAtom == 4) {
                        nextAtomDirection.add(new Number3D(DrawUtils.X_SHIFT, 0f, 0f));
                    } else if (exitAtom == 4 || entryAtom == 1) {
                        nextAtomDirection.subtract(new Number3D(DrawUtils.X_SHIFT, 0f, 0f));
                    }
                    return nextAtomDirection;
                }

                if (entryAtom <= 6 && entryAtom > 4 ||
                        exitAtom < 4 && exitAtom > 1) {
                    nextAtomDirection.add(DrawUtils.SOUTH);
                    if (exitAtom == 1 || entryAtom == 4) {
                        nextAtomDirection.add(new Number3D(DrawUtils.X_SHIFT, 0f, 0f));
                    } else if (exitAtom == 4 || entryAtom == 1) {
                        nextAtomDirection.subtract(new Number3D(DrawUtils.X_SHIFT, 0f, 0f));
                    }
                    return nextAtomDirection;
                }

                if (exitAtom == 1 && entryAtom == 4) {
                    nextAtomDirection = DrawUtils.EAST;
                    return nextAtomDirection;
                }

                if (exitAtom == 4 && entryAtom == 1) {
                    nextAtomDirection = DrawUtils.WEST;
                    return nextAtomDirection;
                }
            } else {

                if (exitAtom == 1 && (entryAtom == 4 || entryAtom == 3)) {
                    nextAtomDirection = DrawUtils.EAST;
                    return nextAtomDirection;
                }

                if (exitAtom == 4 && entryAtom == 1) {
                    nextAtomDirection = DrawUtils.WEST;
                    return nextAtomDirection;
                }

                if (entryAtom < 4 && entryAtom > 1 ||
                        exitAtom <= 6 && exitAtom > 4) {
                    nextAtomDirection = DrawUtils.NORTH;
                    if (exitAtom == 1 || entryAtom == 3) {
                        nextAtomDirection.add(new Number3D(DrawUtils.X_SHIFT, 0f, 0f));
                    } else if (exitAtom == 4 || entryAtom == 1) {
                        nextAtomDirection.subtract(new Number3D(DrawUtils.X_SHIFT, 0f, 0f));
                    }
                    return nextAtomDirection;
                }

                if (entryAtom <= 5 && entryAtom >= 4 ||
                        exitAtom < 4 && exitAtom > 1) {
                    nextAtomDirection = DrawUtils.SOUTH;
                    if (exitAtom == 1 || entryAtom == 4) {
                        nextAtomDirection.add(new Number3D(DrawUtils.X_SHIFT, 0f, 0f));
                    } else if (exitAtom == 4 || entryAtom == 1) {
                        nextAtomDirection.subtract(new Number3D(DrawUtils.X_SHIFT, 0f, 0f));
                    }
                    return nextAtomDirection;
                }
            }
        } else {
            if (entryRingSize == 6) {

                if (exitAtom == 1 && entryAtom == 4) {
                    nextAtomDirection = DrawUtils.EAST;
                    return nextAtomDirection;
                }

                if ((exitAtom == 4 || exitAtom == 3) && entryAtom == 1) {
                    nextAtomDirection = DrawUtils.WEST;
                    return nextAtomDirection;
                }

                if (entryAtom < 4 && entryAtom > 1 ||
                        exitAtom <= 5 && exitAtom >= 4) {
                    nextAtomDirection = DrawUtils.NORTH;
                    if (exitAtom == 1 || entryAtom == 4) {
                        nextAtomDirection.add(new Number3D(DrawUtils.X_SHIFT, 0f, 0f));
                    } else if (exitAtom == 4 || entryAtom == 1) {
                        nextAtomDirection.subtract(new Number3D(DrawUtils.X_SHIFT, 0f, 0f));
                    }
                    return nextAtomDirection;
                }

                if (entryAtom <= 6 && entryAtom > 4 ||
                        exitAtom < 4 && exitAtom > 1) {
                    nextAtomDirection = DrawUtils.SOUTH;
                    if (exitAtom == 1 || entryAtom == 4) {
                        nextAtomDirection.add(new Number3D(DrawUtils.X_SHIFT, 0f, 0f));
                    } else if (exitAtom == 3 || entryAtom == 1) {
                        nextAtomDirection.subtract(new Number3D(DrawUtils.X_SHIFT, 0f, 0f));
                    }
                    return nextAtomDirection;
                }
            } else {

                if (exitAtom == 1 && (entryAtom == 4 || entryAtom == 3)) {
                    nextAtomDirection = DrawUtils.EAST;
                    return nextAtomDirection;
                }

                if ((exitAtom == 4 || exitAtom == 3) && entryAtom == 1) {
                    nextAtomDirection = DrawUtils.WEST;
                    return nextAtomDirection;
                }

                if (entryAtom < 4 && entryAtom > 1 ||
                        exitAtom <= 5 && exitAtom >= 4) {
                    nextAtomDirection = DrawUtils.NORTH;
                    if (exitAtom == 1 || entryAtom == 3) {
                        nextAtomDirection.add(new Number3D(DrawUtils.X_SHIFT, 0f, 0f));
                    } else if (exitAtom == 4 || entryAtom == 1) {
                        nextAtomDirection.subtract(new Number3D(DrawUtils.X_SHIFT, 0f, 0f));
                    }
                    return nextAtomDirection;
                }

                if (entryAtom <= 5 && entryAtom >= 4 ||
                        exitAtom < 4 && exitAtom > 1) {
                    nextAtomDirection = DrawUtils.SOUTH;
                    if (exitAtom == 1 || entryAtom == 4) {
                        nextAtomDirection.add(new Number3D(DrawUtils.X_SHIFT, 0f, 0f));
                    } else if (exitAtom == 3 || entryAtom == 1) {
                        nextAtomDirection.subtract(new Number3D(DrawUtils.X_SHIFT, 0f, 0f));
                    }
                    return nextAtomDirection;
                }
            }
        }
        Log.e(TAG, "Entry/exit combination not accounted for");
        return null;
    }

    private int convertToClockwise(int entryAtom, int ringSize) {
        return Math.abs(entryAtom - ringSize);
    }

    private Plane getIcon(Ring ring, float xPos, float yPos) {
//        Log.e(TAG, residueName + " = x:" + xPos + " y:" + yPos);

        String residueName = ring.getResidueName();

        material = new DiffuseMaterial();
        icon = new Plane(DrawUtils.IMAGE_WIDTH, DrawUtils.IMAGE_HEIGHT, 5, 5);


        icon.setMaterial(material);
        icon.addTexture(mTextureManager.addTexture(ResidueIcons.getIconForResidue(residueName)));
        icon.setTransparent(true);
        icon.setX(xPos);
        icon.setY(yPos);
        icon.setZ(-0.5f);

        while (checkIntersects(ring)) {
            yPos += DrawUtils.Y_SHIFT;
//            xPos += DrawUtils.X_SHIFT;
            icon.setY(yPos);
//            icon.setX(xPos);
            ring.displayPos2D.y = yPos;
//            ring.displayPos2D.x = xPos;
        }

        return icon;
    }

    private boolean checkIntersects(Ring ring) {
        for (Ring otherRing : currentMolecule.getRings()) {
            if (!otherRing.equals(ring) && otherRing.isPositioned()) {
                if (isClose(ring.displayPos2D.x, otherRing.displayPos2D.x) && isClose(ring.displayPos2D.y, otherRing.displayPos2D.y)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isClose(float f1, float f2) {
        return Math.abs(f1 - f2) < (DrawUtils.IMAGE_WIDTH + 0.05f);
    }

    private Line3D getConnector (Stack<Number3D> points) {
        Line3D connection = new Line3D(points, DrawUtils.CONNECTOR_WIDTH, DrawUtils.CONNECTOR_COLOUR);
        SimpleMaterial connectorMat = new SimpleMaterial();
        connection.setMaterial(connectorMat);
        connection.setColor(DrawUtils.CONNECTOR_COLOUR);
        connection.setTransparent(true);

        return connection;
    }

    private void setUpLights() {
        mLight = new DirectionalLight(-1f, -1f, 1.0f); // set the direction
        mLight.setColor(1.0f, 1.0f, 1.0f);
        mLight.setPower(10);
    }

    private void setUpCamera() {
        camera2D = new Camera2D();
        camera2D.setZ(1f);
        setCamera(camera2D);
    }

    @Override
    public void reactToTouchDown(Vector3d touchPos) {
        super.reactToTouchDown(touchPos);

        findMoveBuffer(touchPos, allShapes);
    }

    @Override
    protected void onDoubleTapSwipe(Vector3d position) {
    }

    @Override
    protected void onSwipe(Vector3d position) {
        if (!isScaleInProgress) {
            moveObjectWithBuffer((float) position.x, (float) position.y, allShapes);
        }
    }

    public void reactToScale(float scaleFactor) {
        isScaleInProgress = true;

        scaleFactor = Math.max(0.9f, Math.min(scaleFactor, 1.1f));

        float tempScale = mScale;
        tempScale *= scaleFactor;

        if (tempScale < Constants.MIN_SCALE || tempScale > Constants.MAX_SCALE) {
            return;
        }

        mScale = tempScale;
        allShapes.setScale(new Number3D(mScale, mScale, mScale));
    }

    @Override
    public void setAccelerometerValues(float v, float v1, int i) {

    }

    public static class Renderer2DFactory implements SweetTouchRendererFactory<Renderer2D> {
        public  Renderer2D factory(Context context) {
            return new Renderer2D(context);
        }
    }

}
