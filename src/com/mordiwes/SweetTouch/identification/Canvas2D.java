package com.mordiwes.SweetTouch.identification;

import com.mordiwes.SweetTouch.display.RajawaliFragmentCanvas;
import com.mordiwes.SweetTouch.display.SweetTouchRendererFactory;

/**
 * Created by Wesley on 10/2/2014.
 */
public class Canvas2D extends RajawaliFragmentCanvas<Renderer2D> {
    public static final String TAG = "Canvase2D";

    public Canvas2D() {
        super();
    }

    public Canvas2D(SweetTouchRendererFactory<Renderer2D> rendererFactory, int fragmentLayoutResId) {
        super(rendererFactory, fragmentLayoutResId);
    }

    public void display()
    {
        mRenderer.display();
    }
}
