package com.mordiwes.SweetTouch.display;

import android.content.Context;

/**
 * Created by Mordechai on 2014-10-06.
 */
public interface SweetTouchRendererFactory<T> {

    public T factory(Context context);
}
