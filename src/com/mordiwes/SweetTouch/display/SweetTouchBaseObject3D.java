package com.mordiwes.SweetTouch.display;

import android.opengl.GLES20;
import android.opengl.GLU;
import android.opengl.Matrix;
import rajawali.BaseObject3D;
import rajawali.Camera;
import rajawali.bounds.BoundingBox;
import rajawali.materials.ColorPickerMaterial;
import rajawali.util.ObjectColorPicker;

/**
 * Subclass of BaseObject3D for the sole reason of allowing me to manually control rotation properly.
 * I'm sure the Rajawali library does allow for rotations on more than one axes to be handled, if this is discovered
 * it should be the preference.
 *
 * Also mProjMatrix was always null for setting to screen coords so in this class the Renderer projection matrix is used.
 * Created by Mordechai on 2014-10-05.
 */
public class SweetTouchBaseObject3D extends BaseObject3D {

    private final float[] mTemporaryMatrix = new float[16];
    /** Store the accumulated rotation. */
    private final float[] mAccumulatedRotation = new float[16];

    public SweetTouchBaseObject3D ()
    {
        super();
        Matrix.setIdentityM(mAccumulatedRotation, 0);
    }

    public float [] getAccumulatedRotationsMatrix ()
    {
        return mAccumulatedRotation;
    }


    /**
     * Maps screen coordinates to object coordinates
     * Modification of rejawali method.
     * Rajawli used this objects prjection matrix, this was always null so renderers should be used.
     * Rajawali negated the y value this has also been changed.
     *
     * @param x
     * @param y
     * @param viewportWidth
     * @param viewportHeight
     * @param eyeZ camera z location
     */
    public void setScreenCoordinates(float x, float y, int viewportWidth, int viewportHeight, float[] projectionMatrix, float eyeZ) {
        float[] r1 = new float[16];
        int[] viewport = new int[] { 0, 0, viewportWidth, viewportHeight };
        float[] modelMatrix = new float[16];
        Matrix.setIdentityM(modelMatrix, 0);

        GLU.gluUnProject(x, viewportHeight - y, 0.0f, modelMatrix, 0, projectionMatrix, 0, viewport, 0, r1, 0);
        setPosition(r1[0] * eyeZ, r1[1] * eyeZ, 0);
    }

    /**
     * Maps screen coordinates to object coordinates
     * @param x
     * @param y
     * @param viewportWidth
     * @param viewportHeight
     * @param eyeZ camera z location
     */
    public void shiftByScreenCoordinates(float x, float y, int viewportWidth, int viewportHeight, float[] projectionMatrix, float eyeZ) {
        float[] r1 = new float[16];
        int[] viewport = new int[] { 0, 0, viewportWidth, viewportHeight };
        float[] modelMatrix = new float[16];
        Matrix.setIdentityM(modelMatrix, 0);

        GLU.gluUnProject(x, viewportHeight - y, 0.0f, modelMatrix, 0, projectionMatrix, 0, viewport, 0, r1, 0);
        setPosition(mPosition.x + (r1[0] * eyeZ), mPosition.y + (r1[1] * eyeZ), 0);
    }

    public void shiftByWorldCoordinates(float x, float y, float z) {
        setPosition(mPosition.x + x, mPosition.y +y, mPosition.z + z);
    }



    /**
     * Renders the object
     *
     * @param camera
     *            The camera
     * @param projMatrix
     *            The projection matrix
     * @param vMatrix
     *            The view matrix
     * @param parentMatrix
     *            This object's parent matrix
     * @param pickerInfo
     *            The current color picker info. This is only used when an object is touched.
     */
    public void render(Camera camera, float[] projMatrix, float[] vMatrix, final float[] parentMatrix,
                       ObjectColorPicker.ColorPickerInfo pickerInfo) {
        if (!mIsVisible && !mRenderChildrenAsBatch)
            return;

        preRender();

        // -- move view matrix transformation first
        Matrix.setIdentityM(mMMatrix, 0);
        Matrix.setIdentityM(mScalematrix, 0);
        Matrix.scaleM(mScalematrix, 0, mScale.x, mScale.y, mScale.z);

        Matrix.setIdentityM(mRotateMatrix, 0);

        setOrientation();
        if (mLookAt == null) {
            mOrientation.toRotationMatrix(mRotateMatrix);
        } else {
            System.arraycopy(getLookAt(), 0, mRotateMatrix, 0, 16);
        }

        Matrix.translateM(mMMatrix, 0, mPosition.x, mPosition.y, mPosition.z);
        Matrix.setIdentityM(mTmpMatrix, 0);
        Matrix.multiplyMM(mTmpMatrix, 0, mMMatrix, 0, mScalematrix, 0);
        Matrix.multiplyMM(mMMatrix, 0, mTmpMatrix, 0, mRotateMatrix, 0);
        if (parentMatrix != null) {
            Matrix.multiplyMM(mTmpMatrix, 0, parentMatrix, 0, mMMatrix, 0);
            System.arraycopy(mTmpMatrix, 0, mMMatrix, 0, 16);
        }
        Matrix.multiplyMM(mMVPMatrix, 0, vMatrix, 0, mMMatrix, 0);
        Matrix.multiplyMM(mMVPMatrix, 0, projMatrix, 0, mMVPMatrix, 0);

        applyAccumulatedRotations();// location of this is meaningful

        mIsInFrustum = true; // only if mFrustrumTest == true it check frustum
        if (mFrustumTest && mGeometry.hasBoundingBox()) {
            BoundingBox bbox = mGeometry.getBoundingBox();
            bbox.transform(mMMatrix);
            if (!camera.mFrustum.boundsInFrustum(bbox)) {
                mIsInFrustum = false;
            }
        }

        if (!mIsContainerOnly && mIsInFrustum) {
            mProjMatrix = projMatrix;
            if (!mDoubleSided) {
                GLES20.glEnable(GLES20.GL_CULL_FACE);
                if (mBackSided) {
                    GLES20.glCullFace(GLES20.GL_FRONT);
                } else {
                    GLES20.glCullFace(GLES20.GL_BACK);
                    GLES20.glFrontFace(GLES20.GL_CCW);
                }
            }
            if (mEnableBlending && !(pickerInfo != null && mIsPickingEnabled)) {
                GLES20.glEnable(GLES20.GL_BLEND);
                GLES20.glBlendFunc(mBlendFuncSFactor, mBlendFuncDFactor);
            } else {
                GLES20.glDisable(GLES20.GL_BLEND);
            }
            if (mEnableDepthTest)
                GLES20.glEnable(GLES20.GL_DEPTH_TEST);
            else
                GLES20.glDisable(GLES20.GL_DEPTH_TEST);
            GLES20.glDepthMask(mEnableDepthMask);

            if (pickerInfo != null && mIsPickingEnabled) {
                ColorPickerMaterial pickerMat = pickerInfo.getPicker().getMaterial();
                pickerMat.setPickingColor(mPickingColorArray);
                pickerMat.useProgram();
                pickerMat.setCamera(camera);
                pickerMat.setVertices(mGeometry.getVertexBufferInfo().bufferHandle);
            } else {
                if (!mIsPartOfBatch) {
                    if (mMaterial == null) {
                        throw new RuntimeException(
                                "This object can't renderer because there's no material attached to it.");
                    }
                    mMaterial.useProgram();
                    setShaderParams(camera);
                    mMaterial.bindTextures();
                    mMaterial.setTextureCoords(mGeometry.getTexCoordBufferInfo().bufferHandle, mHasCubemapTexture);
                    mMaterial.setNormals(mGeometry.getNormalBufferInfo().bufferHandle);
                    mMaterial.setCamera(camera);
                    mMaterial.setVertices(mGeometry.getVertexBufferInfo().bufferHandle);
                }
                if (mMaterial.getUseColor())
                    mMaterial.setColors(mGeometry.getColorBufferInfo().bufferHandle);
            }

            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

            if (pickerInfo == null)
            {
                mMaterial.setMVPMatrix(mMVPMatrix);
                mMaterial.setModelMatrix(mMMatrix);
                mMaterial.setViewMatrix(vMatrix);

                if(mIsVisible)
                {
                    GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, mGeometry.getIndexBufferInfo().bufferHandle);
                    GLES20.glDrawElements(mDrawingMode, mGeometry.getNumIndices(), mElementsBufferType,	0);
                    GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
                }
                if (!mIsPartOfBatch && !mRenderChildrenAsBatch) {
                    mMaterial.unbindTextures();
                }
            } else if (pickerInfo != null && mIsPickingEnabled) {
                ColorPickerMaterial pickerMat = pickerInfo.getPicker().getMaterial();
                pickerMat.setMVPMatrix(mMVPMatrix);
                pickerMat.setModelMatrix(mMMatrix);
                pickerMat.setViewMatrix(vMatrix);
                GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, mGeometry.getIndexBufferInfo().bufferHandle);
                GLES20.glDrawElements(mDrawingMode, mGeometry.getNumIndices(), mElementsBufferType,	0);
                GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);

                pickerMat.unbindTextures();
            }
            GLES20.glDisable(GLES20.GL_CULL_FACE);
            GLES20.glDisable(GLES20.GL_BLEND);
            GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        }

        if (mShowBoundingVolume) {
            if (mGeometry.hasBoundingBox())
                mGeometry.getBoundingBox().drawBoundingVolume(camera, projMatrix, vMatrix, mMMatrix);
            if (mGeometry.hasBoundingSphere())
                mGeometry.getBoundingSphere().drawBoundingVolume(camera, projMatrix, vMatrix, mMMatrix);
        }
        // Draw children without frustum test
        for (int i = 0, j = mChildren.size(); i < j; i++)
            mChildren.get(i).render(camera, projMatrix, vMatrix, mMMatrix, pickerInfo);

        if (mRenderChildrenAsBatch) {
            mMaterial.unbindTextures();
        }
    }

    private void applyAccumulatedRotations()
    {
        Matrix.multiplyMM(mTemporaryMatrix, 0, mMMatrix, 0, mAccumulatedRotation, 0);
        System.arraycopy(mTemporaryMatrix, 0, mMMatrix, 0, 16);
    }
}
