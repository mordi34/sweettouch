package com.mordiwes.SweetTouch.display;

import android.app.Activity;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.mordiwes.SweetTouch.R;
import com.mordiwes.SweetTouch.util.Vector3d;
import rajawali.RajawaliFragment;

/**
 * Created by Mordechai on 2014-10-06.
 */
public abstract class RajawaliFragmentCanvas<T extends SweetTouchRenderer> extends RajawaliFragment implements View.OnTouchListener, SensorEventListener {
    protected T mRenderer;
    private float mScaleFactor = 1.f;
    private ScaleGestureDetector mScaleDetector;
    private GestureDetector mGestureDetector;
    private int mFragmentLayoutResId;
    private SweetTouchRendererFactory<T> rendererFactory;

    protected ImageView mLoaderGraphic;

    private final float ALPHA = 0.8f;
    private final int SENSITIVITY = 5;
    private SensorManager mSensorManager;
    private float mGravity[];

    public RajawaliFragmentCanvas(SweetTouchRendererFactory<T> rendererFactory, int fragmentLayoutResId)//eg R.layout.fragment_visualiser
    {
        this.rendererFactory = rendererFactory;
        this.mFragmentLayoutResId = fragmentLayoutResId;
    }

    public RajawaliFragmentCanvas() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRenderer = rendererFactory.factory(getActivity());
        mRenderer.setSurfaceView(mSurfaceView);
        setRenderer(mRenderer);

       mScaleDetector = new ScaleGestureDetector(getActivity().getApplicationContext(), new ScaleListener());
       mGestureDetector = new GestureDetector(getActivity().getApplicationContext(), new GestureListener());

        mGravity = new float[3];
        mSensorManager = (SensorManager) getActivity().getSystemService(Activity.SENSOR_SERVICE);
        mSensorManager.registerListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_FASTEST);

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        mLayout = (FrameLayout)inflater.inflate(mFragmentLayoutResId, container, false);
        mLayout.addView(mSurfaceView,0);// must add it at first child so that the label is above it and visible
        mScaleDetector = new ScaleGestureDetector(getActivity().getApplicationContext(), new ScaleListener());// twice?

        mLayout.setOnTouchListener(this);

        return mLayout;
    }

    public void reset()
    {
        mRenderer.reset();
    }

    public void toggleBackgroundColour()
    {
        mRenderer.toggleBackgroundColour();
    }

    public boolean onTouch(View v, MotionEvent me) {
        mScaleDetector.onTouchEvent(me);
        if(!mScaleDetector.isInProgress()) {
            mGestureDetector.onTouchEvent(me);
        }

        Vector3d touchPos = new Vector3d(me.getX(), me.getY(), 0);

        if (me.getAction() == MotionEvent.ACTION_DOWN) { //Called once at the start of the touch
            mRenderer.reactToTouchDown(touchPos);

        } else if (me.getAction() == MotionEvent.ACTION_UP) { //Called once at the end of the touch

            mRenderer.reactToTouchUp(touchPos);

        } else if (me.getAction() == MotionEvent.ACTION_MOVE) { //Called repeatedly as touch point moves

            if (!mScaleDetector.isInProgress()) {
                mRenderer.reactToSwipe(touchPos);
            }
        }

        try {
            Thread.sleep(15); //Small delay to keep touch events from overflowing and decreasing performance
        } catch (Exception e) {
            //System.out.println("Error with touch " + e);
        }

        return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mLayout != null) {
            mLayout.removeView(mSurfaceView);
        }
    }


    @Override
    public void onDestroy() {
        try {
            super.onDestroy();
        } catch (Exception e) {
        }
        mRenderer.onSurfaceDestroyed();
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

        @Override
        public boolean onScale(ScaleGestureDetector detector) {

            mScaleFactor *= detector.getScaleFactor();

            mRenderer.reactToScale(mScaleFactor);

            mScaleFactor = 1;
            //invalidate();
            return true;
        }
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }
        // event when double tap occurs
        @Override
        public boolean onDoubleTap(MotionEvent me) {
            Vector3d touchPos = new Vector3d(me.getX(), me.getY(), 0);
            return mRenderer.reactToDoubleTap(touchPos);

        }

        @Override
        public void onLongPress(MotionEvent me) {
            Log.d("Long Press", "(" + me.getX() + "," + me.getY() + ")");
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        // ignore orientation/keyboard change
        super.onConfigurationChanged(newConfig);
    }

    public void onAccuracyChanged(Sensor arg0, int arg1) {

    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            mGravity[0] = ALPHA * mGravity[0] + (1 - ALPHA) * event.values[0];
            mGravity[1] = ALPHA * mGravity[1] + (1 - ALPHA) * event.values[1];
            mGravity[2] = ALPHA * mGravity[2] + (1 - ALPHA) * event.values[2];

            mRenderer.setAccelerometerValues(event.values[1] - mGravity[1]
                            * SENSITIVITY,
                    event.values[0] - mGravity[0] * SENSITIVITY, 0);
        }
    }

    public  void showLoader() {
        mLayout.post(new Runnable() {
            public void run() {

//                if(mLoaderGraphic == null)
//                {
                    mLoaderGraphic = new ImageView(getActivity());
                    mLayout.addView(mLoaderGraphic);
              //  }

                mLoaderGraphic.setId(1);
                mLoaderGraphic.setScaleType(ImageView.ScaleType.CENTER);
                mLoaderGraphic.setImageResource(R.drawable.loader);

                AnimationSet animSet = new AnimationSet(false);

                RotateAnimation anim1 = new RotateAnimation(360, 0,
                        Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF,
                        .5f);
                anim1.setRepeatCount(Animation.INFINITE);
                anim1.setDuration(2000);
                animSet.addAnimation(anim1);

                AlphaAnimation anim2 = new AlphaAnimation(0, 1);
                anim2.setRepeatCount(0);
                anim2.setDuration(1000);
//                anim2.setRepeatCount(Animation.INFINITE);// for flashing
                animSet.addAnimation(anim2);

                mLoaderGraphic.setAnimation(animSet);
            }
        });
    }

    public void hideLoader() {
        mLayout.post(new Runnable() {
            public void run() {
                if(mLoaderGraphic == null)
                {
                    return;
                }
                AlphaAnimation anim = new AlphaAnimation(1, 0);
                anim.setRepeatCount(0);
                anim.setDuration(500);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationStart(Animation animation) {
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationEnd(Animation animation) {
                        mLoaderGraphic.setVisibility(View.INVISIBLE);
                        mLayout.removeView(mLoaderGraphic);

                    }
                });
                ((AnimationSet) mLoaderGraphic.getAnimation())
                        .addAnimation(anim);
            }
        });
    }

}
