package com.mordiwes.SweetTouch.display;

import android.opengl.GLES20;
import com.mordiwes.SweetTouch.elements.Atom;
import com.mordiwes.SweetTouch.util.PeriodicTable;
import rajawali.Geometry3D;

/**
 * Modification of the Rajawali Sphere class
 * Created by Mordechai on 2014-09-19.
 */
public class AtomSphere extends SweetTouchBaseObject3D {

    private final float PI = (float) Math.PI;
    private float mRadius;
    private final int CPA_SEGMENT_IMPROVEMENT_FACTOR = 2;
    private int mSegmentsWInitial;
    private int mSegmentsHInitial;
    private int mSegmentsW;
    private int mSegmentsH;
    private Atom mAtom;
    private final int VDW_REDUCTION_FOR_DISPLAY = 5;

    public AtomSphere() {
    super();
    }

    public AtomSphere(float radius, int segmentsW, int segmentsH, Atom atom) {
        super();
        mRadius = radius;
        mSegmentsW = segmentsW;
        mSegmentsH = segmentsH;
        mSegmentsWInitial = segmentsW;
        mSegmentsHInitial = segmentsH;
        mAtom = atom;
        if(atom != null) {
            setToDisplayRadiusPreInit();//in case no radius can be found, does not neccesarily overwrite default radius
        }
        init();
    }

    protected void init() {

        int numVertices = (mSegmentsW + 1) * (mSegmentsH + 1);
        int numIndices = 2 * mSegmentsW * (mSegmentsH - 1) * 3;

        float[] vertices = new float[numVertices * 3];
        float[] normals = new float[numVertices * 3];
        float[] colors = new float[numVertices * 4];
        int[] indices = new int[numIndices];

        int i, j;
        int vertIndex = 0, index = 0;
        final float normLen = 1.0f / mRadius;

        for (j = 0; j <= mSegmentsH; ++j) {
            float horAngle = PI * j / mSegmentsH;
            float z = mRadius * (float) Math.cos(horAngle);
            float ringRadius = mRadius * (float) Math.sin(horAngle);

            for (i = 0; i <= mSegmentsW; ++i) {
                float verAngle = 2.0f * PI * i / mSegmentsW;
                float x = ringRadius * (float) Math.cos(verAngle);
                float y = ringRadius * (float) Math.sin(verAngle);

                normals[vertIndex] = x * normLen;
                vertices[vertIndex++] = x;
                normals[vertIndex] = z * normLen;
                vertices[vertIndex++] = z;
                normals[vertIndex] = y * normLen;
                vertices[vertIndex++] = y;

                if (i > 0 && j > 0) {
                    int a = (mSegmentsW + 1) * j + i;
                    int b = (mSegmentsW + 1) * j + i - 1;
                    int c = (mSegmentsW + 1) * (j - 1) + i - 1;
                    int d = (mSegmentsW + 1) * (j - 1) + i;

                    if (j == mSegmentsH) {
                        indices[index++] = a;
                        indices[index++] = c;
                        indices[index++] = d;
                    } else if (j == 1) {
                        indices[index++] = a;
                        indices[index++] = b;
                        indices[index++] = c;
                    } else {
                        indices[index++] = a;
                        indices[index++] = b;
                        indices[index++] = c;
                        indices[index++] = a;
                        indices[index++] = c;
                        indices[index++] = d;
                    }
                }
            }
        }

        int numUvs = (mSegmentsH + 1) * (mSegmentsW + 1) * 2;
        float[] textureCoords = new float[numUvs];

        numUvs = 0;
        for (j = 0; j <= mSegmentsH; ++j) {
            for (i = 0; i <= mSegmentsW; ++i) {
                textureCoords[numUvs++] = -(float) i / mSegmentsW;
                textureCoords[numUvs++] = (float) j / mSegmentsH;
            }
        }

        int numColors = numVertices * 4;
        for (j = 0; j < numColors; j += 4) {
            colors[j] = 1.0f;
            colors[j + 1] = 0;
            colors[j + 2] = 0;
            colors[j + 3] = 1.0f;
        }

        setData(vertices, normals, textureCoords, colors, indices);
        if (mAtom != null) {
            setToCpkColour();
        }
    }

    public void setmAtom (Atom atom)
    {
        mAtom = atom;
    }


    public void setRadius(float radius) {
        mRadius = radius;
        mGeometry = new Geometry3D();//have this or bufferoverflow will occur.
        init();
    }

    public void setToVdwRadius() {
        try {
            mSegmentsH *=CPA_SEGMENT_IMPROVEMENT_FACTOR;
            mSegmentsW *=CPA_SEGMENT_IMPROVEMENT_FACTOR;
            setRadius(PeriodicTable.getVanDeWaalRadius(mAtom));
        } catch (IllegalArgumentException ex) {
        }

    }

    public void setToDisplayRadiusPostInit() throws IllegalArgumentException {
        mSegmentsH = mSegmentsHInitial;
        mSegmentsW = mSegmentsWInitial;
        setRadius(PeriodicTable.getVanDeWaalRadius(mAtom) / VDW_REDUCTION_FOR_DISPLAY);
    }

    private void setToDisplayRadiusPreInit() {
        try {
            mRadius = PeriodicTable.getVanDeWaalRadius(mAtom) / VDW_REDUCTION_FOR_DISPLAY;
        } catch (IllegalArgumentException ex) {
        }
    }

    public void setToCpkColour() {
        setColor(PeriodicTable.getColour(mAtom));
    }


    public AtomSphere clone() {
        return clone(true);
    }

    public AtomSphere clone(boolean copyMaterial) {
        AtomSphere clone = new AtomSphere();
        cloneTo(clone, copyMaterial);
        clone.setRotation(getRotation());
        clone.setScale(getScale());
        return clone;
    }


    protected void cloneTo(AtomSphere clone, boolean copyMaterial) {
        clone.getGeometry().copyFromGeometry3D(mGeometry);
        clone.isContainer(mIsContainerOnly);
        if (copyMaterial)
            clone.setMaterial(mMaterial, false);
        clone.mElementsBufferType = mGeometry.areOnlyShortBuffersSupported() ? GLES20.GL_UNSIGNED_SHORT
                : GLES20.GL_UNSIGNED_INT;
        clone.mTransparent = this.mTransparent;
        clone.mEnableBlending = this.mEnableBlending;
        clone.mBlendFuncSFactor = this.mBlendFuncSFactor;
        clone.mBlendFuncDFactor = this.mBlendFuncDFactor;
        clone.mEnableDepthTest = this.mEnableDepthTest;
        clone.mEnableDepthMask = this.mEnableDepthMask;
    }
}
