package com.mordiwes.SweetTouch.display;

import android.content.Context;
import android.opengl.GLU;
import android.opengl.Matrix;
import com.mordiwes.SweetTouch.util.Vector3d;
import rajawali.math.Number3D;
import rajawali.renderer.RajawaliRenderer;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by Mordechai on 2014-10-06.
 */
public abstract class SweetTouchRenderer extends RajawaliRenderer  {

    protected float mSwipeDeltaX = 0f;
    protected float mSwipeDeltaY = 0f;
    protected float mScale = 1f;
    protected Vector3d mLastTouch = new Vector3d(0, 0, 0);

    protected SweetTouchBaseObject3D mSelectedObject;

    protected boolean flagNewRequestToReset = false;
    protected boolean newRequestToToggleBackgroundColour = false;

    protected boolean isDoubleTapInProgress;
    protected boolean isScaleInProgress = false;
    protected boolean isFirstTouch = true;

    protected Vector3d originShift;
    protected Vector3d moveBuffer;

    protected int mBackgroundColourToggleCount = 0;
    float [][] backgroundColours = {{0,0,0,1},{0.85f,0.85f,0.85f,1f}};


    public SweetTouchRenderer(Context context, int frameRate) {
        super(context);
        setFrameRate(frameRate);
    }

    public void reset() {
        flagNewRequestToReset = true;
        originShift = null;
    }

    public void toggleBackgroundColour()
    {
        newRequestToToggleBackgroundColour = true;
    }

    public void onDrawFrame(final GL10 glUnused)
    {
        super.onDrawFrame(glUnused);

        if (newRequestToToggleBackgroundColour)
        {
            newRequestToToggleBackgroundColour = false;
            mBackgroundColourToggleCount++;
            setBackgroundColor(backgroundColours[mBackgroundColourToggleCount %backgroundColours.length][0],backgroundColours[mBackgroundColourToggleCount %backgroundColours.length][1],backgroundColours[mBackgroundColourToggleCount %backgroundColours.length][2],backgroundColours[mBackgroundColourToggleCount %backgroundColours.length][3]);

        }
    }

    /**
     * Called when a swipe occurs in the containing class. Makes calls to
     * onSwipe or onDoubleTapSwipe as appropriate. Generally the aforementioned are overriden while this method is not.
     *
     * Accomodation for other swipes should be made in this fashion.
     * @param position
     */
    public void reactToSwipe(Vector3d position){

        if (isDoubleTapInProgress) {
            onDoubleTapSwipe(position);
        } else if (!isFirstTouch) {
            onSwipe(position);
        } else {
            isFirstTouch = false;
        }

        mLastTouch = position;
    }

    protected abstract void onDoubleTapSwipe(Vector3d position);

    protected abstract void onSwipe(Vector3d position);

    public void reactToTouchDown(Vector3d position) {
        isFirstTouch = true;
    }

    public void reactToTouchUp(Vector3d position) {
        stopCurrentlyMovingObject();
        isDoubleTapInProgress = false;
        isScaleInProgress = false;
        isFirstTouch = true;
    }

    public boolean reactToDoubleTap(Vector3d position) {
        isDoubleTapInProgress = true;
        isFirstTouch = true;

        return true;
    }

    public abstract void reactToScale(float scaleFactor);

    public void moveObjectTo(float x, float y, SweetTouchBaseObject3D object) {
       if (object == null ) {
            return;
       }

        mSelectedObject = object;

        //use the one in SweetTouchBaseObject3D
        mSelectedObject.setScreenCoordinates(x, y, mViewportWidth, mViewportHeight, mCamera.getProjectionMatrix(), mCamera.getZ());
    }

    public void moveObjectWithBuffer(float x, float y, SweetTouchBaseObject3D object)
    {
        if (object == null ) {
            return;
        }

        mSelectedObject = object;

        Vector3d moveInWorldCoords = new Vector3d(getInWorldCoordinates(x, y));

        mSelectedObject.setPosition((float) (moveInWorldCoords.x - moveBuffer.x), (float) (moveInWorldCoords.y - moveBuffer.y), mSelectedObject.getZ());

    }

    public void stopCurrentlyMovingObject() {
        mSelectedObject = null;
    }

    public Number3D getInWorldCoordinates (Vector3d point)
    {
        return getInWorldCoordinates ((float)point.x, (float) point.y, mViewportWidth, mViewportHeight, mCamera.getProjectionMatrix(), mCamera.getZ());
    }

    public Number3D getInWorldCoordinates (Number3D point)
    {
        return getInWorldCoordinates (point.x, point.y, mViewportWidth, mViewportHeight, mCamera.getProjectionMatrix(), mCamera.getZ());
    }

    public Number3D getInWorldCoordinates (float x, float y)
    {
        return getInWorldCoordinates(x, y, mViewportWidth, mViewportHeight, mCamera.getProjectionMatrix(), mCamera.getZ());
    }

    /**
     * Maps screen coordinates to object coordinates.
     *
     * @param x
     * @param y
     * @param viewportWidth
     * @param viewportHeight
     * @param eyeZ camera z location
     */
    public Number3D getInWorldCoordinates(float x, float y, int viewportWidth, int viewportHeight, float[] projectionMatrix, float eyeZ) {
        float[] r1 = new float[16];
        int[] viewport = new int[] { 0, 0, viewportWidth, viewportHeight };
        float[] modelMatrix = new float[16];
        Matrix.setIdentityM(modelMatrix, 0);

        GLU.gluUnProject(x, viewportHeight - y, 0.0f, modelMatrix, 0, projectionMatrix, 0, viewport, 0, r1, 0);
        return new Number3D(r1[0] * eyeZ, r1[1] * eyeZ, 0);
    }


    protected void findOriginShift(Vector3d intialPoint) {
        originShift = findShiftToPoint(intialPoint, new Vector3d(0, 0, 0));
    }

    protected void findMoveBuffer(Vector3d touchPoint, SweetTouchBaseObject3D movingObject) {
        moveBuffer = findBufferToPoint(new Vector3d(getInWorldCoordinates(touchPoint)), new Vector3d(movingObject.getPosition()));
    }

    protected Vector3d findBufferToPoint(Vector3d intialPoint, Vector3d point) {
        return new Vector3d(findBufferForAxes(intialPoint.x, point.x), findBufferForAxes(intialPoint.y, point.y), findBufferForAxes(intialPoint.z, point.z));
    }

    protected double findBufferForAxes(double initialChord, double coord) {
        double newCoord = initialChord - coord;
        return newCoord;
    }

    protected Vector3d findShiftToPoint(Vector3d intialPoint, Vector3d point) {
        return new Vector3d(findShiftForAxes(intialPoint.x, point.x), findShiftForAxes(intialPoint.y, point.y), findShiftForAxes(intialPoint.z, point.z));
    }

    protected double findShiftForAxes(double initialChord, double coord) {
        double newCoord = initialChord - coord;

        newCoord = Math.abs(newCoord);

        if (initialChord > coord) {
            newCoord *= -1;
        }

        return newCoord;
    }

    protected Number3D calculateShiftedPosition(Vector3d point, Vector3d shift) {
        return new Number3D((float) point.x + shift.x, (float) point.y + shift.y, (float) point.z + shift.z);
    }

    protected Number3D calculateShiftedPosition(Number3D point, Vector3d shift) {
        Vector3d toLoc = new Vector3d(point);
        return calculateShiftedPosition(toLoc, shift);
    }

    public abstract void setAccelerometerValues(float v, float v1, int i);


    public abstract static class RendererFactory implements SweetTouchRendererFactory<SweetTouchRenderer> {
        public abstract SweetTouchRenderer factory();
    }

}
